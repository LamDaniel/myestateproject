package myestatepackage;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * ContractorTest to test Contractor Class
 * @author 1932789
 */
public class ContractorTest {
    
    Contractor test = new Contractor(1, "testContractor", "testCompany", "5141234567", "contractor@gmail.com", "Electrician");
    /**
     * Test of getContractorId method, of class Contractor.
     */
    @Test
    public void testGetContractorId() {
        int expResult = 1;
        int result = test.getContractorId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFullName method, of class Contractor.
     */
    @Test
    public void testGetFullName() {

        String expResult = "testContractor";
        String result = test.getFullName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCompanyName method, of class Contractor.
     */
    @Test
    public void testGetCompanyName() {
        String expResult = "testCompany";
        String result = test.getCompanyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhoneNumber method, of class Contractor.
     */
    @Test
    public void testGetPhoneNumber() {
        String expResult = "5141234567";
        String result = test.getPhoneNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmailAddress method, of class Contractor.
     */
    @Test
    public void testGetEmailAddress() {
        String expResult = "contractor@gmail.com";
        String result = test.getEmailAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProfession method, of class Contractor.
     */
    @Test
    public void testGetProfession() {

        String expResult = "Electrician";
        String result = test.getProfession();
        assertEquals(expResult, result);
    }
    
}
