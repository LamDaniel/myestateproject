package myestatepackage;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * LeaseTest test lease class
 * @author 1932789
 */
public class LeaseTest {
    
    Lease test = new Lease(2, "2021-06-16", "2022-07-16");
    /**
     * Test of getStartDate method, of class Lease.
     */
    @Test
    public void testGetStartDate() {

        String expResult = "2021-06-16";
        String result = test.getStartDate();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEndDate method, of class Lease.
     */
    @Test
    public void testGetEndDate() {

        String expResult = "2022-07-16";
        String result = test.getEndDate();
        assertEquals(expResult, result);

    }

    /**
     * Test of getLeaseId method, of class Lease.
     */
    @Test
    public void testGetLeaseId() {

        int expResult = 2;
        int result = test.getLeaseId();
        assertEquals(expResult, result);

    }
}
