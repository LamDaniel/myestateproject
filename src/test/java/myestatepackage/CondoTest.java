package myestatepackage;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * CondoTest tests Condo class
 * @author 1932789
 */
public class CondoTest {

    Property test = new Condo("testPropertyName", "House", "testAddress", 100.50, 200.25, 10.5, 20.5, "testInsuranceName", 0.50, 50.49);
    /**
     * Test of getCondoFees method, of class Condo.
     */
    @Test
    public void testGetCondoFees() {
 
        double expResult = 50.49;
        double result = test.getCondoFees();
        assertEquals(expResult, result, 0.0);

    }
}
