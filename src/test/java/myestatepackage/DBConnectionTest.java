package myestatepackage;

import java.sql.Connection;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * DBConnection class to test DBConnection if connection works
 * @author Daniel Lam (1932789)
 */
public class DBConnectionTest {
    
    public DBConnectionTest() {
    }

    /**
     * Test of getConnection method, of class DBConnection.
     */
    @Test
    public void testGetConnection() {
        System.out.println("getConnection");
        Connection result = DBConnection.getConnection();
        if (result == null) {
            fail("Connection have failed!");
        }
        else {
            //It works!
        }
    }
    
}
