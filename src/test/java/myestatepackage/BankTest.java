/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myestatepackage;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * BankTest to test Bank class
 * @author Daniel Lam (1932789)
 */
public class BankTest {
    
    Bank bankTest = new Bank(1, "bankTest", 10.5);
    Bank bankTest2 = new Bank(1, "bankTest", 10.5);
    Bank bankTest3 = new Bank(2, "differentBank", 0);
    /**
     * Test of getName method, of class Bank.
     */
    @Test
    public void testGetName() {
        String expected = "bankTest";
        String result = bankTest.getName();
        assertEquals(expected,result);
    }

    /**
     * Test of getInterestRate method, of class Bank.
     */
    @Test
    public void testGetInterestRate() {
        double expected = 10.5;
        double result = bankTest.getInterestRate();
        assertEquals(expected,result);
    }

    /**
     * Test of toString method, of class Bank.
     */
    @Test
    public void testToString() {
        String expected = "bankTest: 10.5%";
        String result = bankTest.toString();
        assertEquals(expected, result);
    }
    
    /**
     * Test of equals(), of class Bank
     */
    @Test
    public void testEquals() {
        boolean expected = true;
        boolean result = bankTest.equals(bankTest2);
        boolean expectedFail = false;
        boolean failResult = bankTest.equals(bankTest3);
        
        assertEquals(expected, result);
        assertEquals(expectedFail, failResult);
    }
    
}
