package myestatepackage;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * PlexTest tests Plex Class
 * @author 1932789
 */
public class PlexTest {

    Property test = new Plex("testPropertyName", "House", "testAddress", 100.50, 200.25, 10.5, 20.5, "testInsuranceName", 0.50, 420);
    /**
     * Test of getNberOfUnits method, of class Plex.
     */
    @Test
    public void testGetNberOfUnits() {

        int expResult = 420;
        int result = test.getNberOfUnits();
        assertEquals(expResult, result);

    }
}
