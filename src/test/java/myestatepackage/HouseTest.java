package myestatepackage;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * HouseTest tests House class
 * @author 1932789
 */
public class HouseTest {
    
    Property propertyTest = new House("testPropertyName", "House", "testAddress", 100.50, 200.25, 10.5, 20.5, "testInsuranceName", 0.50);
    
    /**
     * Test of getPropertyName method, of class Property.
     */
    @Test
    public void testGetPropertyName() {
        String expected = "testPropertyName";
        String result = propertyTest.getPropertyName();
        assertEquals(expected, result);
    }

    /**
     * Test of getType method, of class Property.
     */
    @Test
    public void testGetType() {
        String expected = "House";
        String result = propertyTest.getType();
        assertEquals(expected, result);
    }

    /**
     * Test of getAddress method, of class Property.
     */
    @Test
    public void testGetAddress() {
        String expected = "testAddress";
        String result = propertyTest.getAddress();
        assertEquals(expected, result);
    }

    /**
     * Test of getCost method, of class Property.
     */
    @Test
    public void testGetCost() {
        double expected = 100.50;
        double result = propertyTest.getValue();
        assertEquals(expected, result);
    }

    /**
     * Test of getRentalPrice method, of class Property.
     */
    @Test
    public void testGetRentalPrice() {
        double expected = 200.25;
        double result = propertyTest.getRentalPrice();
        assertEquals(expected, result);
    }

    /**
     * Test of getPropertyTaxRate method, of class Property.
     */
    @Test
    public void testGetPropertyTaxRate() {
        double expected = 10.50;
        double result = propertyTest.getPropertyTaxRate();
        assertEquals(expected, result);
    }

    /**
     * Test of getSchoolTaxRate method, of class Property.
     */
    @Test
    public void testGetSchoolTaxRate() {
        double expected = 20.50;
        double result = propertyTest.getSchoolTaxRate();
        assertEquals(expected, result);
    }

    /**
     * Test of getInsuranceName method, of class Property.
     */
    @Test
    public void testGetInsuranceName() {
        String expected = "testInsuranceName";
        String result = propertyTest.getInsuranceName();
        assertEquals(expected, result);
    }

    /**
     * Test of getInterestRate method, of class Property.
     */
    @Test
    public void testGetInterestRate() {
        double expected = 0.5;
        double result = propertyTest.getInterestRate();
        assertEquals(expected, result);
    }
}
