package myestatepackage;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * RenovationTest test Renovation class
 * @author 1932789
 */
public class RenovationTest {

    Renovation test = new Renovation(1, "testProperty", "Jane Doe", 500.69, "Electricity", "Power Outage", false);
    /**
     * Test of getRenovationId method, of class Renovation.
     */
    @Test
    public void testGetRenovationId() {

        int expResult = 1;
        int result = test.getRenovationId();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPropertyName method, of class Renovation.
     */
    @Test
    public void testGetPropertyName() {

        String expResult = "testProperty";
        String result = test.getPropertyName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getContractorName method, of class Renovation.
     */
    @Test
    public void testGetContractorName() {

        String expResult = "Jane Doe";
        String result = test.getContractorName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCost method, of class Renovation.
     */
    @Test
    public void testGetCost() {

        double expResult = 500.69;
        double result = test.getCost();
        assertEquals(expResult, result);

    }

    /**
     * Test of getType method, of class Renovation.
     */
    @Test
    public void testGetType() {

        String expResult = "Electricity";
        String result = test.getType();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDescription method, of class Renovation.
     */
    @Test
    public void testGetDescription() {

        String expResult = "Power Outage";
        String result = test.getDescription();
        assertEquals(expResult, result);

    }

    /**
     * Test of getIsFinished method, of class Renovation.
     */
    @Test
    public void testGetIsFinished() {

        boolean expResult = false;
        boolean result = test.getIsFinished();
        assertEquals(expResult, result);

    }
}
