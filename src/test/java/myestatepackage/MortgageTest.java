package myestatepackage;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * MortgageTest to test Mortgage class
 * @author Daniel Lam (1932789)
 */
public class MortgageTest {

    Mortgage instance = new Mortgage("testPropertyName", "2021-04-25", "2021-05-25", "testDate", 500.50);
    
    /**
     * Test of getPropertyName method, of class Mortgage.
     */
    @Test
    public void testGetPropertyName() {
        String expected = "testPropertyName";
        String result = instance.getPropertyName();
        assertEquals(expected, result);
    }

    /**
     * Test of getPaymentDate method, of class Mortgage.
     */
    @Test
    public void testGetPaymentDate() {
        String expected = "2021-04-25";
        String result = instance.getPaymentDate();
        assertEquals(expected, result);
    }

    /**
     * Test of getStartDate method, of class Mortgage.
     */
    @Test
    public void testGetStartDate() {
        String expected = "2021-05-25";
        String result = instance.getStartDate();
        assertEquals(expected, result);
    }

    /**
     * Test of getEndDate method, of class Mortgage.
     */
    @Test
    public void testGetEndDate() {
        String expected = "2021-04-25";
        String result = instance.getPaymentDate();
        assertEquals(expected, result);
    }

    /**
     * Test of getDownPayment method, of class Mortgage.
     */
    @Test
    public void testGetDownPayment() {
        double expected = 500.50;
        double result = instance.getDownPayment();
        assertEquals(expected, result);
    }
}
