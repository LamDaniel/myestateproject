/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myestatepackage;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author 1932789
 */
public class TenantTest {
    
    Lease leaseObj = new Lease(1, "2021-05-22", "2022-05-22");
    Tenant test = new Tenant(1, "John Doe", "testProperty", "5141234567", "johndoe@gmail.com", "2021-05-16", "2021-06-16", 100.50, 0.50, true, "Debit", leaseObj);
    /**
     * Test of getFullName method, of class Tenant.
     */
    @Test
    public void testGetFullName() {
        
        String expResult = "John Doe";
        String result = test.getFullName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyName method, of class Tenant.
     */
    @Test
    public void testGetPropertyName() {

        String expResult = "testProperty";
        String result = test.getPropertyName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPhoneNumber method, of class Tenant.
     */
    @Test
    public void testGetPhoneNumber() {

        String expResult = "5141234567";
        String result = test.getPhoneNumber();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEmailAddress method, of class Tenant.
     */
    @Test
    public void testGetEmailAddress() {

        String expResult = "johndoe@gmail.com";
        String result = test.getEmailAddress();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRentStartDate method, of class Tenant.
     */
    @Test
    public void testGetRentStartDate() {

        String expResult = "2021-05-16";
        String result = test.getRentStartDate();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRentEndDate method, of class Tenant.
     */
    @Test
    public void testGetRentEndDate() {

        String expResult = "2021-06-16";
        String result = test.getRentEndDate();
        assertEquals(expResult, result);

    }

    /**
     * Test of getYearlyIncome method, of class Tenant.
     */
    @Test
    public void testGetYearlyIncome() {

        double expResult = 100.50;
        double result = test.getYearlyIncome();
        assertEquals(expResult, result);

    }

    /**
     * Test of getIncomeDebtRatio method, of class Tenant.
     */
    @Test
    public void testGetIncomeDebtRatio() {

        double expResult = 0.50;
        double result = test.getIncomeDebtRatio();
        assertEquals(expResult, result);

    }

    /**
     * Test of getHasPaidRent method, of class Tenant.
     */
    @Test
    public void testGetHasPaidRent() {

        boolean expResult = true;
        boolean result = test.getHasPaidRent();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPaymentType method, of class Tenant.
     */
    @Test
    public void testGetPaymentType() {

        String expResult = "Debit";
        String result = test.getPaymentType();
        assertEquals(expResult, result);

    }

    /**
     * Test of getTenantId method, of class Tenant.
     */
    @Test
    public void testGetTenantId() {

        int expResult = 1;
        int result = test.getTenantId();
        assertEquals(expResult, result);

    }

    /**
     * Test of getTenantLease method, of class Tenant.
     */
    @Test
    public void testGetTenantLease() {

        Lease expResult = leaseObj;
        Lease result = test.getTenantLease();
        assertEquals(expResult, result);

    }
    
}
