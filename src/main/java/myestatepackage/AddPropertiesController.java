package myestatepackage;

import java.io.IOException;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

/**
 * AddPropertiesController shows form and methods to add properties 
 * @author 1932789
 */
public class AddPropertiesController extends PropertiesForm {  
    
    /**
     * confirmProperty() creates property
     * @throws IOException 
     */
    @FXML
    private void confirmProperty() throws IOException {
        createTemplate();
    }
    
    /**
     * create() create property object and changes view to addMortgages
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        Property propertyObj = createProperty();
        System.out.println(propertyObj);
        FXMLLoader fxmlloader = new FXMLLoader(App.class.getResource("addMortgages.fxml"));
        Parent parent = fxmlloader.load();
        AddMortgagesController addMortgagesController = (AddMortgagesController) fxmlloader.getController();
        addMortgagesController.setProperty(propertyObj);
        addMortgagesController.displayPropertyName(propertyObj.getPropertyName());
        App.setRoot(parent);
    }
    /**
     * createProperty() creates property based on the property type
     * @return 
     */
    private Property createProperty() {
        Property propertyObj = null;
        switch (typeCB.getValue()) {
            case "Condo":
                propertyObj = new Condo(propertyNameTf.getText(), typeCB.getValue(), addressTf.getText(), formatAmountOrRate(valueTf), formatAmountOrRate(rental_priceTf), formatAmountOrRate(property_tax_rateTf), formatAmountOrRate(school_tax_rateTf), insuranceNameTf.getText(), formatAmountOrRate(interestRateTf), formatAmountOrRate(condoFeesTf));
                break;
            case "Plexes":
                propertyObj = new Plex(propertyNameTf.getText(), typeCB.getValue(), addressTf.getText(), formatAmountOrRate(valueTf), formatAmountOrRate(rental_priceTf), formatAmountOrRate(property_tax_rateTf), formatAmountOrRate(school_tax_rateTf), insuranceNameTf.getText(), formatAmountOrRate(interestRateTf), Integer.parseInt(nberOfUnitsTf.getText()));
                break;
            case "House":
                propertyObj = new House(propertyNameTf.getText(), typeCB.getValue(), addressTf.getText(), formatAmountOrRate(valueTf), formatAmountOrRate(rental_priceTf), formatAmountOrRate(property_tax_rateTf), formatAmountOrRate(school_tax_rateTf), insuranceNameTf.getText(), formatAmountOrRate(interestRateTf));
                break;
            default:
                break;
        }
        return propertyObj;
    }
}
