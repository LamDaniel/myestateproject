/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * RenovationForm class contains all fields and methods of a renovation form
 * @author Daniel Lam (1932789)
 */
public abstract class RenovationForm extends FormUtilities implements Initializable {
    
    //Variables
    @FXML private TextField propertyNameTf;
    @FXML private TextField contractorNameTf;
    @FXML private TextField costTf;
    @FXML private TextField typeTf;
    @FXML private TextArea descriptionTa;
    @FXML private ChoiceBox isFinishedCB;
    ObservableList<String> isFinished = FXCollections.observableArrayList("Yes", "No");
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadData();
    }
    /**
     * loadData() sets items of lists into choice boxes
     */
    private void loadData() {
         isFinishedCB.getItems().setAll(isFinished);
    }
    
    /**
     * returnToRenovations() changes view to renovations
     * @throws IOException 
     */
    @FXML
    private void returnToRenovations() throws IOException {
        App.setRoot("renovations");
    }
    /**
     * createTemplate() calls create() and catches any errors
     */
    protected void createTemplate() {
        try {
            conn.setAutoCommit(false);
            create();
            conn.setAutoCommit(true);
            returnToRenovations();
        }
        catch (NullPointerException|NumberFormatException e) {
            showErrorAlert("Can't be created", "Renovation can't be created or modified.", "Please recheck for any missing cases and incorrect formats.");
            e.printStackTrace();
        }
        catch (SQLIntegrityConstraintViolationException e){
            showErrorAlert("Invalid Property or/and Contractor Name", "Property or/and Contractor name couldn't be found.", "Please retype the correct Property or/and Contractor Name (including capital letters)");
            e.printStackTrace();
        }
        catch (SQLException e) {
            showErrorAlert("Database error", "Error while inserting into database.", "Please recheck for any incorrect formats and try again.");
            e.printStackTrace();
        }
        catch (Exception e) {
            showErrorAlert("Unknown Error", "An Unknown Error Occured.", "Please try again.");
            e.printStackTrace();
        }
    }
    
    /**
     * create() is abstract method that runs the methods to create property
     * @throws SQLException
     * @throws IOException 
     */
    protected abstract void create() throws SQLException, IOException;
    
    //Getters
    public TextField getPropertyNameTf() {
        return propertyNameTf;
    }
    public TextField getContractorNameTf() {
        return contractorNameTf;
    }
    public TextField getCostTf() {
        return costTf;
    }
    public TextField getTypeTf() {
        return typeTf;
    }       
    public TextArea getDescriptionTa() {
        return descriptionTa;
    }
    public ChoiceBox getIsFinishedCB() {
        return isFinishedCB;
    }
    
    /**
     * getContractorId() returns appropriate id of contractor
     * @return
     * @throws SQLException 
     */
    protected int getContractorId() throws SQLException {
        int contractorId = 0;
        String contractorIdStmt = "SELECT contractor_id FROM Contractors WHERE name = ?";
        PreparedStatement ps = conn.prepareStatement(contractorIdStmt);
        ps.setString(1, getContractorNameTf().getText());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            contractorId = rs.getInt("contractor_id");
        }
        return contractorId;
    }
    
    /**
     * getContractorId() returns appropriate id of property
     * @return
     * @throws SQLException 
     */
    protected int getPropertyId() throws SQLException {
        int propertyId = 0;
        String contractorIdStmt = "SELECT property_id FROM Properties WHERE propertyName = ?";
        PreparedStatement ps = conn.prepareStatement(contractorIdStmt);
        ps.setString(1, getPropertyNameTf().getText());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            propertyId = rs.getInt("property_id");
        }
        return propertyId;        
    }
}
