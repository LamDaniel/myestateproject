package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * RenovationsController is the controller that displays all Renovations and allows user to add, delete and modify Renovations
 * @author Daniel Lam (1932789)
 */
public class RenovationsController extends MainController implements Initializable {
    
    //Variables
    @FXML private TableView<Renovation> renovationsTableView;
    @FXML private TableColumn<Renovation, String> propertyNameCol;
    @FXML private TableColumn<Renovation, String> contractorNameCol;
    @FXML private TableColumn<Renovation, String> typeCol;
    @FXML private TableColumn<Renovation, Double> costCol;
    @FXML private TableColumn<Renovation, String> descriptionCol;
    @FXML private TableColumn<Renovation, Boolean> isFinishedCol;
    ObservableList<Renovation> renovationsList = FXCollections.observableArrayList();
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
           loadRenovationsFromDB();
           renovationsTableView.setItems(renovationsList);
           initializeColumns();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * loadTenantsFromDB() adds Renovation objects from database into ObservableList
     * @throws SQLException 
     */
    private void loadRenovationsFromDB() throws SQLException {
        String loadRenovationsStmt = "SELECT * FROM Renovations r INNER JOIN Properties p ON p.property_id = r.property_id INNER JOIN Contractors c ON c.contractor_id = r.contractor_id;";
        PreparedStatement ps = conn.prepareStatement(loadRenovationsStmt);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Renovation renovationObj = new Renovation(rs.getInt("renovation_id"), rs.getString("propertyName"), rs.getString("name"), rs.getDouble("cost"), rs.getString("renovation_type"), rs.getString("description"), rs.getBoolean("isFinished"));
            System.out.println(renovationObj);
            renovationsList.add(renovationObj);
        }
    }
    /**
     * initializeColumns() initializes columns of tenantsTableView
     */
    private void initializeColumns() {
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<>("propertyName"));
        contractorNameCol.setCellValueFactory(new PropertyValueFactory<>("contractorName"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        costCol.setCellValueFactory(new PropertyValueFactory<>("cost"));
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        isFinishedCol.setCellValueFactory(new PropertyValueFactory<>("isFinished"));
    }
    
    /**
     * switchToAddRenovations() changes view to addRenovations
     * @throws IOException 
     */
    @FXML
    private void switchToAddRenovations() throws IOException {
        App.setRoot("addRenovations");
    }
    
    /**
     * switchToViewRenovations() checks if Renovation has been selected and changes view to viewRenovations
     * @throws IOException 
     */
    @FXML
    private void switchToViewRenovations() throws IOException {
        Renovation renovationObj = renovationsTableView.getSelectionModel().getSelectedItem();
        if (renovationObj == null) {
            showErrorAlert("No Selected Renovation", "Please select a Renovation first.", "");
        }
        else {
            FXMLLoader fxmlloader = new FXMLLoader(App.class.getResource("viewRenovations.fxml"));
            Parent parent = fxmlloader.load();
            
            ViewRenovationsController viewRenovationsController = (ViewRenovationsController) fxmlloader.getController();
            viewRenovationsController.setRenovation(renovationObj);
            viewRenovationsController.displayFields(renovationObj);
            App.setRoot(parent);
        }
    }
    
    /**
     * confirmRemove() checks if Renovation has been selected and removes it
     * @throws IOException 
     */
    @FXML
    private void confirmRemove() throws IOException {
        Renovation renovationObj = renovationsTableView.getSelectionModel().getSelectedItem();
        if (renovationObj == null) {
            showErrorAlert("No Selected Renovation", "Please select a Renovation first.", "");
        }
        else {
            if (confirm("Confirm Remove", "Do you really want to delete this Renovation?")) {
                try {
                    removeRenovationFromDB(renovationObj);
                    switchToRenovations();
                }
                catch (SQLException e) {
                    showErrorAlert("Database Error", "Error deleting from database.", "Please try again later.");
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * removeRenovationFromDB() takes selected renovation object and removes it from database
     * @param renovationObj
     * @throws SQLException 
     */
    private void removeRenovationFromDB(Renovation renovationObj) throws SQLException{
        conn.setAutoCommit(false);
        String removeContractorFromDB = "DELETE FROM Renovations WHERE renovation_id = ?";
        PreparedStatement ps = conn.prepareStatement(removeContractorFromDB);
        ps.setInt(1, renovationObj.getRenovationId());
        ps.execute();
        conn.setAutoCommit(true);
    }
}
