package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXML;

/**
 * ViewRenovationsController is the controller of modifying the Renovation
 * @author 1932789
 */
public class ViewRenovationsController extends RenovationForm {
    
    //Variables
    private Renovation renovationObj;
    
    /**
     * confirmModify() checks if user wants to modify
     */
    @FXML
    private void confirmModify() {
        if (confirm("Modify Renovation", "Do you really want to modify this Renovation?")) {
            createTemplate();
        }
    }
    
    /**
     * setRenovation() sets renovationObj to controller
     * @param renovationObj 
     */
    public void setRenovation(Renovation renovationObj) {
        this.renovationObj = renovationObj;
    }
    /**
     * displayFields() displays all fields of renovationObj
     * @param renovationObj 
     */
    public void displayFields(Renovation renovationObj) {
        getPropertyNameTf().setText(renovationObj.getPropertyName());
        getContractorNameTf().setText(renovationObj.getContractorName());
        getCostTf().setText(renovationObj.getCost() + "");
        getTypeTf().setText(renovationObj.getType());
        getDescriptionTa().setText(renovationObj.getDescription());
        getIsFinishedCB().setValue(translateBooleanToString(renovationObj.getIsFinished()));
    }
    
    /**
     * create() updates renovation
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        int contractorId = getContractorId();
        int propertyId = getPropertyId();
        
        String updateRenovationStmt = "UPDATE Renovations SET contractor_id = ?, property_id = ?, cost = ?, renovation_type = ?, description = ?, isFinished = ? WHERE renovation_id = ?";
        PreparedStatement ps = conn.prepareStatement(updateRenovationStmt);
        ps.setInt(1, contractorId);
        ps.setInt(2, propertyId);
        ps.setDouble(3, formatAmountOrRate(getCostTf()));
        ps.setString(4, getTypeTf().getText());
        ps.setString(5, getDescriptionTa().getText());
        ps.setBoolean(6, translateToBoolean(getIsFinishedCB()));
        ps.setInt(7, renovationObj.getRenovationId());
        ps.execute();    
    }
}