package myestatepackage;

/**
 * Renovation class contains all the fields, getters and setters of a Renovation Object
 * @author Daniel Lam (1932789)
 */
public class Renovation {
    
    //Variables
    private int renovationId;
    private String type;
    private double cost;
    private String description;
    private boolean isFinished;
    private String propertyName;
    private String contractorName;
    
    //Constructor with renovationId
    public Renovation(int renovationId, String propertyName, String contractorName, double cost, String type, String description, boolean isFinished) {
        this.renovationId = renovationId;
        this.propertyName = propertyName;
        this.contractorName = contractorName;
        this.cost = cost;
        this.type = type;
        this.description = description;
        this.isFinished = isFinished;
    }
    
    //Constructor without renovationId
    public Renovation(String propertyName, String contractorName, double cost, String type, String description, boolean isFinished) {
        this.propertyName = propertyName;
        this.contractorName = contractorName;
        this.cost = cost;
        this.type = type;
        this.description = description;
        this.isFinished = isFinished;
    }
    
    //Getters
    public int getRenovationId() {
        return renovationId;
    }
    public String getPropertyName() {
        return propertyName;
    }
    public String getContractorName() {
        return contractorName;
    }
    public double getCost() {
        return cost;
    }
    public String getType() {
        return type;
    }
    public String getDescription() {
        return description;
    }
    public boolean getIsFinished() {
        return isFinished;
    }
    
    //toString() displays the Renovation's fields when invoked
    @Override
    public String toString() {
        return "RENOVATION: " + propertyName + ", " + contractorName + ", " + cost + ", " + description + ", " + isFinished + " - ID: " + renovationId;
    }
}
