package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * MortgageForm class contains all fields and methods of a mortgage form
 * @author Daniel Lam (1932789)
 */
public abstract class MortgageForm extends FormUtilities implements Initializable {
    
    //Variables
    @FXML private TextField propertyNameTf;
    @FXML private DatePicker paymentDatePicker;
    @FXML private DatePicker startDatePicker;
    @FXML private DatePicker endDatePicker;
    @FXML private TextField downPaymentTf;
    @FXML private ChoiceBox<Bank> bankCB;
    private Property propertyObj;
    private Mortgage mortgageObj;
    ObservableList list = FXCollections.observableArrayList();
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            displayBanks();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * displayBanks() displays all banks into choicebox
     * @throws SQLException 
     */
    private void displayBanks() throws SQLException {
        String allBanksQuery = "SELECT * FROM bank_institution;";
        PreparedStatement stmt = conn.prepareStatement(allBanksQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Bank bankObj = new Bank(rs.getInt("bank_id"), rs.getString("institution_name"), rs.getDouble("interest_rate"));
            list.add(bankObj);
        }
        bankCB.setItems(list);
    }
    
    /**
     * createTemplate() calls create() and catches any errors
     */
    protected void createTemplate() {
        try {
            conn.setAutoCommit(false);
            create();
            conn.setAutoCommit(true);
            switchToMortgages();
        }
        catch (NullPointerException|NumberFormatException e) {
            showErrorAlert("Can't be created", "Mortgage can't be created or modified.", "Please recheck for any missing cases and incorrect formats.");
            e.printStackTrace();
        }
        catch (SQLIntegrityConstraintViolationException e) {
            showErrorAlert("Existing Property", "Property already exists in the database.", "Please name your property differently.");
            e.printStackTrace();
        }
        catch (SQLException e) {
            showErrorAlert("Database error", "Error while inserting into database.", "Please recheck for any incorrect formats and try again.");
            e.printStackTrace();
        }
        catch (Exception e) {
            showErrorAlert("Unknown Error", "An Unknown Error Occured.", "Please try again.");
            e.printStackTrace();
        }
    }
    
    /**
     * create() is abstract method that runs the methods to create property
     * @throws SQLException
     * @throws IOException 
     */
    protected abstract void create() throws SQLException, IOException;
    
    public void setProperty(Property propertyObj) {
        this.propertyObj = propertyObj;
    }
    public void setMortgage(Mortgage mortgageObj) {
        this.mortgageObj = mortgageObj;
    }
    
    //Getters
    public TextField getPropertyNameTf() {
        return propertyNameTf;
    }
    public DatePicker getPaymentDatePicker() {
        return paymentDatePicker;
    }
    public DatePicker getStartDatePicker() {
        return startDatePicker;
    }
    public DatePicker getEndDatePicker() {
        return endDatePicker;
    }
    public TextField getDownPaymentTf() {
        return downPaymentTf;
    }
    public ChoiceBox<Bank> getBankCB() {
        return bankCB;
    }
    public Property getPropertyObj() {
        return propertyObj;
    }
    public Mortgage getMortgageObj() {
        return mortgageObj;
    }
}
