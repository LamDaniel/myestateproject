package myestatepackage;

import java.io.IOException;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * ContractorForm class contains all fields and methods of a contractor form
 * @author Daniel Lam (1932789)
 */
public abstract class ContractorForm extends FormUtilities {
    
    //Variables
    @FXML private TextField contractorNameTf;
    @FXML private TextField companyNameTf;
    @FXML private TextField phoneNumberTf;
    @FXML private TextField emailAddressTf;
    @FXML private TextField professionTf;
    
    /**
     * createTemplate() calls create method with all catch clauses for Exceptions
     */
    protected void createTemplate() {
        try {
            conn.setAutoCommit(false);
            create();
            conn.setAutoCommit(true);
            App.setRoot("contractors");
        }
        catch (NullPointerException|NumberFormatException e) {
            showErrorAlert("Can't be created", "Contractor can't be created or modified.", "Please recheck for any missing cases and incorrect formats.");
            e.printStackTrace();
        }
        catch (SQLException e) {
            showErrorAlert("Database error", "Error while inserting into database.", "Please recheck for any incorrect formats and try again.");
            e.printStackTrace();
        }
        catch (Exception e) {
            showErrorAlert("Unknown Error", "An Unknown Error Occured.", "Please try again.");
            e.printStackTrace();
        }
    }
    
    /**
     * create() is abstract method that runs the methods to create property
     * @throws SQLException
     * @throws IOException 
     */
    protected abstract void create() throws SQLException, IOException;
    
    /**
     * returnToContractors() changes views to contractors
     * @throws IOException 
     */
    @FXML
    protected void returnToContractors() throws IOException {
        App.setRoot("contractors");
    }
    
    //Getters
    public TextField getContractorNameTf() {
        return contractorNameTf;
    }
    public TextField getCompanyNameTf() {
        return companyNameTf;
    }
    public TextField getPhoneNumberTf() {
        return phoneNumberTf;
    }
    public TextField getEmailAddressTf() {
        return emailAddressTf;
    }
    public TextField getProfessionTf() {
        return professionTf;
    }
}
