package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXML;

/**
 * ViewContractorsController is the controller of modifying the Contractor
 * @author 1932789
 */
public class ViewContractorsController extends ContractorForm {
    
    //Variables
    private Contractor contractorObj;
    
    /**
     * confirmModify() confirms if user wants to modify Contractor
     */
    @FXML
    private void confirmModify() {
        if (confirm("Confirm Modify", "Do you really want to modify this Contractor?")) {
            createTemplate();
        }
    }
    /**
     * create() updates Contractor
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        updateContractor();
        returnToContractors();
    }
    /**
     * updateContractor() updates contractor in DB
     * @throws SQLException 
     */
    private void updateContractor() throws SQLException {
        String updateContractorStmt = "UPDATE Contractors SET name = ?, company = ?, phone_number = ?, email = ?, profession = ? WHERE contractor_id = ?";
        PreparedStatement ps = conn.prepareStatement(updateContractorStmt);
        ps.setString(1, getContractorNameTf().getText());
        ps.setString(2, getCompanyNameTf().getText());
        ps.setString(3, formatPhoneNumber(getPhoneNumberTf()));
        ps.setString(4, getEmailAddressTf().getText());
        ps.setString(5, getProfessionTf().getText());
        ps.setInt(6, contractorObj.getContractorId());
        ps.execute();
    }
    
    /**
     * setContractor() takes contractor Object and sets it
     * @param contractorObj 
     */
    public void setContractor(Contractor contractorObj) {
        this.contractorObj = contractorObj;
    }
    /**
     * displayFields() displays fields of contractorObj to view
     * @param contractorObj 
     */
    public void displayFields(Contractor contractorObj) {
        getContractorNameTf().setText(contractorObj.getFullName());
        getCompanyNameTf().setText(contractorObj.getCompanyName());
        getPhoneNumberTf().setText(contractorObj.getPhoneNumber());
        getEmailAddressTf().setText(contractorObj.getEmailAddress());
        getProfessionTf().setText(contractorObj.getProfession());
    }
}
