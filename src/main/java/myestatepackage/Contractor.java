package myestatepackage;

/**
 * Contractor class contains all the fields, getters and setters of a Contractor Object
 * @author 1932789
 */
public class Contractor {
    
    //Variables
    private int contractorId;
    private String fullName;
    private String companyName;
    private String phoneNumber;
    private String emailAddress;
    private String profession;
    
    //Constructor without contractorId
    public Contractor(String fullName, String companyName, String phoneNumber, String emailAddress, String profession) {
        this.fullName = fullName;
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.profession = profession;
    }
    
    //Constructor with contractorId
    public Contractor(int contractorId, String fullName, String companyName, String phoneNumber, String emailAddress, String profession) {
        this.contractorId = contractorId;
        this.fullName = fullName;
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.profession = profession;
    }
    
    //Getters
    public int getContractorId() {
        return contractorId;
    }
    public String getFullName() {
        return fullName;
    }
    public String getCompanyName() {
        return companyName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public String getProfession() {
        return profession;
    }
    
    //toString() displays all the fields of a Contractor object as a String
    @Override
    public String toString() {
        return "CONTRACTOR: " + fullName + ", " + companyName + ", " + phoneNumber + ", " + emailAddress + ", " + profession + " - ID: " + contractorId;
    }
}
