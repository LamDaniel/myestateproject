package myestatepackage;

/**
 * Property class contains all the fields, getters and setters of a Property Object
 * @author 1932789
 */
public abstract class Property {
    
    //Variables
    private String propertyName;
    private String type;
    private String address;
    private double value;
    private double rentalPrice;
    private double propertyTaxRate;
    private double schoolTaxRate;
    private String insuranceName;
    private double interestRate;
    private int propertyId;
    
    //Constructor with propertyId
    public Property(int propertyId, String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate) {
        this.propertyId = propertyId;
        this.propertyName = propertyName;
        this.type = type;
        this.address = address;
        this.value = value;
        this.rentalPrice = rentalPrice;
        this.propertyTaxRate = propertyTaxRate;
        this.schoolTaxRate = schoolTaxRate;
    }
    
    //Constructor without propertyId
    public Property(int propertyId, String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate) {
        this.propertyId = propertyId;
        this.propertyName = propertyName;
        this.type = type;
        this.address = address;
        this.value = value;
        this.rentalPrice = rentalPrice;
        this.propertyTaxRate = propertyTaxRate;
        this.schoolTaxRate = schoolTaxRate;
        this.insuranceName = insuranceName;
        this.interestRate = interestRate;
    }
    
    //Constructor without propertyId, insuranceName and insuranceRate
    public Property(String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate) {
        this.propertyName = propertyName;
        this.type = type;
        this.address = address;
        this.value = value;
        this.rentalPrice = rentalPrice;
        this.propertyTaxRate = propertyTaxRate;
        this.schoolTaxRate = schoolTaxRate;
    }
    
    //Constructor with propertyId, but without insuranceName and insuranceRate
    public Property(String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate) {
        this.propertyName = propertyName;
        this.type = type;
        this.address = address;
        this.value = value;
        this.rentalPrice = rentalPrice;
        this.propertyTaxRate = propertyTaxRate;
        this.schoolTaxRate = schoolTaxRate;
        this.insuranceName = insuranceName;
        this.interestRate = interestRate;
    }
    
    //Getters
    public String getPropertyName() {
        return propertyName;
    }
    public String getType() {
        return type;
    }
    public String getAddress() {
        return address;
    }
    public double getValue() {
        return value;
    }
    public double getRentalPrice() {
        return rentalPrice;
    }
    public double getPropertyTaxRate() {
        return propertyTaxRate;
    }
    public double getSchoolTaxRate() {
        return schoolTaxRate;
    }
    public String getInsuranceName() {
        return insuranceName;
    }
    public double getInterestRate() {
        return interestRate;
    }
    public int getNberOfUnits() {
        return 1; //default number of units
    }
    public double getCondoFees() {
        return 0; //default condo fees
    }
    public int getPropertyId() {
        return propertyId;
    }
    
    /**
     * toString() displays all the fields of the Property object
     * @return 
     */
    @Override
    public String toString() {
        return "PROPERTY: " + propertyName + ", " + type + ", " + address + ", " + value + ", " + rentalPrice + ", " + propertyTaxRate + ", " + schoolTaxRate + ", " + insuranceName + ", " + interestRate + " - ID: " + propertyId;
    }
}
