 package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

 /**
 * AddMortgagesController shows form and methods to add mortgages 
 * @author 1932789
 */
public class AddMortgagesController extends MortgageForm implements Initializable {
    
    /**
     * returnToAddProperties() returns view to add Properties
     * @throws IOException 
     */
    @FXML
    private void returnToAddProperties() throws IOException {
        App.setRoot("addProperties");
    }
    
    /**
     * createProperty() creates property
     * @throws SQLException
     * @throws IOException 
     */
    @FXML
    private void createProperty() throws SQLException, IOException { 
        createTemplate();
        switchToProperties();
    }
    
    /**
     * create() creates mortgage obj
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        Mortgage mortgageObj = new Mortgage(this.getPropertyObj().getPropertyName(), formatDate(getPaymentDatePicker()), formatDate(getStartDatePicker()), formatDate(getEndDatePicker()), formatAmountOrRate(getDownPaymentTf()));
        setMortgage(mortgageObj);
            
        System.out.println(getPropertyObj().toString());
        System.out.println(mortgageObj.toString());
        insertAllIntoDB();
    }
    
    /**
     * insertAllIntoDB() is a helper method to add insurance, mortgage and property into database
     * @throws SQLException 
     */
    private void insertAllIntoDB() throws SQLException {
        addInsuranceIntoDB();
        addMortgageIntoDB();
        addPropertyIntoDB();
        conn.commit();
    }
    
    /**
     * addMortgageIntoDB() adds mortgage into DB
     * @throws SQLException 
     */
    private void addMortgageIntoDB() throws SQLException {
        int bankId = findBankId();
                
        String insertMortgageQuery = "INSERT INTO mortgage (mortgage_id, bank_id, payment_date, start_date, end_date, down_payment) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(insertMortgageQuery);
        ps.setString(1, null);
        ps.setInt(2, bankId);
        ps.setString(3, getMortgageObj().getPaymentDate());
        ps.setString(4, getMortgageObj().getStartDate());
        ps.setString(5, getMortgageObj().getEndDate());
        ps.setDouble(6, getMortgageObj().getDownPayment());
        ps.execute();
        System.out.println("Added Mortgage.");
    }
    /**
     * findBankId() find bank id of correct bank
     * @return
     * @throws SQLException 
     */
    private int findBankId() throws SQLException {
        int bankId = 0;
        String findBankIdQuery = "SELECT bank_id FROM bank_institution WHERE institution_name = ?";
        PreparedStatement psId = conn.prepareStatement(findBankIdQuery);
        psId.setString(1, getBankCB().getValue().getName());
        ResultSet rs = psId.executeQuery();
        while (rs.next()) {
            bankId = rs.getInt("bank_id");
        }
        return bankId;
    }
    /**
     * addInsuranceIntoDB() adds insurance into database
     * @throws SQLException 
     */
    private void addInsuranceIntoDB() throws SQLException {
        String insertInsuranceQuery = "INSERT INTO insurance (insurance_id, name, interest_rate) VALUES (?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(insertInsuranceQuery);
        ps.setString(1, null);
        ps.setString(2, getPropertyObj().getInsuranceName());
        ps.setDouble(3, getPropertyObj().getInterestRate());
        ps.execute();
        System.out.println("Added Insurance");
    }
    /**
     * addPropertyIntoDB() adds property into database
     * @throws SQLException 
     */
    private void addPropertyIntoDB() throws SQLException {
        int insuranceId = findInsuranceId();
        int mortgageId = findMortgageId();
        
        String insertPropertyQuery = "INSERT INTO properties (property_id, insurance_id, mortgage_id, propertyName, type, address, value, nber_units, rental_price, property_tax_rate, school_tax_rate, condo_fees) VALUES (?, ?, ?, ?, ?, ?, ?, IFNULL(?, 1), ?, ?, ?, IFNULL(?, 0))";
        PreparedStatement ps = conn.prepareStatement(insertPropertyQuery);
        ps.setString(1, null);
        ps.setInt(2, insuranceId);
        ps.setInt(3, mortgageId);
        ps.setString(4, getPropertyObj().getPropertyName());
        ps.setString(5, getPropertyObj().getType());
        ps.setString(6, getPropertyObj().getAddress());
        ps.setDouble(7, getPropertyObj().getValue());
        try {
            ps.setInt(8, getPropertyObj().getNberOfUnits());
        }
        catch (Exception e) {
            ps.setString(8, null);
        }
        ps.setDouble(9, getPropertyObj().getRentalPrice());
        ps.setDouble(10, getPropertyObj().getPropertyTaxRate());
        ps.setDouble(11, getPropertyObj().getSchoolTaxRate());
        try {
            ps.setDouble(12, getPropertyObj().getCondoFees());
        }
        catch (Exception e) {
            ps.setString(12, null); 
        }
        ps.execute();
        System.out.println("Added Property.");
    }
    /**
     * findInsuranceId() returns id of correct insurance
     * @throws SQLException 
     */
    private int findInsuranceId() throws SQLException {
        int insuranceId = 0;
        String findInsuranceIdQuery = "SELECT insurance_id FROM insurance WHERE name = ?";
        PreparedStatement ps = conn.prepareStatement(findInsuranceIdQuery);
        ps.setString(1, this.getPropertyObj().getInsuranceName());
        ResultSet rs = ps.executeQuery();
        while(rs.next()) {
            insuranceId = rs.getInt("insurance_id");
        }
        return insuranceId;
    }
   /**
     * findMortgageId() returns id of correct mortgage
     * @throws SQLException 
     */
    private int findMortgageId() throws SQLException {
        int mortgageId = 0;
        String findInsuranceIdQuery = "SELECT last_insert_id()";
        PreparedStatement ps = conn.prepareStatement(findInsuranceIdQuery);
        ResultSet rs = ps.executeQuery();
        while(rs.next()) {
            mortgageId = rs.getInt("last_insert_id()");
        }
        return mortgageId;
    }
    
    /**
     * displayPropertyName displays property name into textfield
     * @param propertyName 
     */
    public void displayPropertyName(String propertyName) {
        getPropertyNameTf().setText(propertyName);
    }
}

