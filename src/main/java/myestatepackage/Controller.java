package myestatepackage;

import java.io.IOException;

/**
 * Controller interface displays all the necessary methods for a Controller class
 * @author 1932789
 */
public interface Controller {
    public void switchToProperties() throws IOException;
    public void switchToTenants() throws IOException;
    public void switchToMortgages() throws IOException;
    public void switchToRenovations() throws IOException;
    public void switchToContractors() throws IOException;
    public void switchToMain() throws IOException;
}
