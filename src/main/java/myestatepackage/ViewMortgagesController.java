package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXML;

/**
 * ViewMortgagesController is the controller of modifying the Mortgage
 * @author 1932789
 */
public class ViewMortgagesController extends MortgageForm {
    
    //Variables
    private Mortgage mortgageObj;
    
    /**
     * modifyMortgage() checks if user wants to modify mortgage
     * @throws IOException 
     */
    @FXML
    private void modifyMortgage() throws IOException {
        if (confirm("Modify Mortgage", "Do you really want to modify this mortgage?")) {
            createTemplate();
        }
    }
    
    /**
     * returnToMortgage() returns to mortgages view
     * @throws IOException 
     */
    @FXML
    private void returnToMortgages() throws IOException {
        App.setRoot("mortgages");
    }
    
    /**
     * create() updates mortgage
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        String updateMortgageStmt = "UPDATE Mortgage SET bank_id = ?, payment_date = ?, start_date = ?, end_date = ?, down_payment = ? WHERE mortgage_id = ?";
        PreparedStatement ps = conn.prepareStatement(updateMortgageStmt);
        ps.setInt(1, getBankCB().getValue().getBankId());
        ps.setString(2, formatDate(getPaymentDatePicker()));
        ps.setString(3, formatDate(getStartDatePicker()));
        ps.setString(4, formatDate(getEndDatePicker()));
        ps.setDouble(5, formatAmountOrRate(getDownPaymentTf()));
        ps.setInt(6, mortgageObj.getMortgageId());
        ps.execute();
    }
    
    /**
     * setMortgage() sets mortgageObj to controller
     * @param mortgageObj 
     */
    @Override
    public void setMortgage(Mortgage mortgageObj) {
        this.mortgageObj = mortgageObj;
    }
    
    /**
     * dispalyFields() displays fields of mortgageObj to view
     * @param mortgageObj 
     */
    public void displayFields(Mortgage mortgageObj) {
        System.out.println(mortgageObj.getBank().getBankId());
        getPropertyNameTf().setText(mortgageObj.getPropertyName());
        getPaymentDatePicker().setValue(formatStringToDate(mortgageObj.getPaymentDate()));
        getStartDatePicker().setValue(formatStringToDate(mortgageObj.getStartDate()));
        getEndDatePicker().setValue(formatStringToDate(mortgageObj.getEndDate()));
        getDownPaymentTf().setText(mortgageObj.getDownPayment() + "");
        getBankCB().setValue(mortgageObj.getBank());
    }
}
