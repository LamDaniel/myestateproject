package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;

/**
 * ViewTenantsController is controller of modifying the tenant
 * @author Daniel Lam (1932789)
 */
public class ViewTenantsController extends TenantsForm {
    
    //Variables
    private Tenant tenantObj;
    
    /**
     * modifyTenant() displays confirm message and if user chooses yes, modifies the tenant
     * @throws SQLException
     * @throws IOException 
     */
    @FXML
    private void modifyTenant() throws SQLException, IOException {
        if (confirm("Confirm Modify", "Do you really want to modify this Tenant?")) {
            createTemplate();
        }
    }
    
    /**
     * create() updates Lease and Tenant
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
         updateAll();
         App.setRoot("tenants");
    }
    /**
     * updateAll() is a helper method to separate updating lease and tenant
     * @throws SQLException 
     */
    private void updateAll() throws SQLException {
        updateLease();
        updateTenant();
    }
    /**
     * updateLease() updates Lease in Database
     * @throws SQLException 
     */
    private void updateLease() throws SQLException {
        int leaseId = getLeaseId();
        
        String leaseStmt = "UPDATE Lease SET start_date = ?, end_date = ? WHERE lease_id = ?";
        PreparedStatement ps = conn.prepareStatement(leaseStmt);
        ps.setString(1, formatDate(leaseStartDatePicker));
        ps.setString(2, formatDate(leaseEndDatePicker));
        ps.setInt(3, leaseId);
        ps.execute();
        System.out.println("Updated Lease.");
    }
    /**
     * getLeaseId() is a helper method that returns the lease_id corresponding with the tenant 
     * @return int leaseId
     * @throws SQLException 
     */
    private int getLeaseId() throws SQLException {
        int leaseId = 0;
        String leaseIdStmt = "SELECT lease_id FROM Lease WHERE tenant_id = ?";
        PreparedStatement ps = conn.prepareStatement(leaseIdStmt);
        ps.setInt(1, this.tenantObj.getTenantId());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            leaseId = rs.getInt("lease_id");
        }
        return leaseId;
    }
    /**
     * updateTenant() updates Tenants in Database
     * @throws SQLException 
     */
    private void updateTenant() throws SQLException {
        String tenantStmt = "UPDATE Tenants SET fullname = ?, phone_number = ?, email = ?, rent_start_date = ?, rent_end_date = ?, yearly_income = ?, income_debt_ratio = ?, hasPaidRent = ?, payment_type = ? WHERE tenant_id = ?";
        PreparedStatement ps = conn.prepareStatement(tenantStmt);
        ps.setString(1, fullNameTf.getText());
        ps.setString(2, formatPhoneNumber(phoneNumberTf));
        ps.setString(3, emailTf.getText());
        ps.setString(4, formatDate(rentStartDatePicker));
        ps.setString(5, formatDate(rentEndDatePicker));
        ps.setDouble(6, formatAmountOrRate(yearlyIncomeTf));
        ps.setDouble(7, formatAmountOrRate(incomeDebtRatioTf));
        ps.setBoolean(8, translateToBoolean(hasPaidRentCB));
        ps.setString(9, paymentTypeCB.getValue().toString());
        ps.setInt(10, this.tenantObj.getTenantId());
        ps.execute();
        System.out.println("Updated Tenant.");
    }
    
    /**
     * displayFields() shows all of the tenant's fields in the form
     * @param fullName
     * @param propertyName
     * @param email
     * @param phone
     * @param yearlyIncome
     * @param paymentType
     * @param incomeDebtRatio
     * @param rentStart
     * @param rentEnd
     * @param leaseStart
     * @param leaseEnd
     * @param hasPaidRent 
     */
    public void displayFields(String fullName, String propertyName, String email, String phone, double yearlyIncome, String paymentType, double incomeDebtRatio, String rentStart, String rentEnd, String leaseStart, String leaseEnd, boolean hasPaidRent) {
        fullNameTf.setText(fullName);
        propertyNameTf.setText(propertyName);
        emailTf.setText(email);
        phoneNumberTf.setText(phone);
        yearlyIncomeTf.setText(yearlyIncome + "");
        paymentTypeCB.setValue(paymentType);
        incomeDebtRatioTf.setText(incomeDebtRatio + "");
        rentStartDatePicker.setValue(formatStringToDate(rentStart));
        rentEndDatePicker.setValue(formatStringToDate(rentEnd));
        leaseStartDatePicker.setValue(formatStringToDate(leaseStart));
        leaseEndDatePicker.setValue(formatStringToDate(leaseEnd));
        hasPaidRentCB.setValue(translateBooleanToString(hasPaidRent));
    }
    
    /**
     * setTenant() sets the tenantcObj to this controller passed by the previous controller
     * @param tenantObj 
     */
    public void setTenant(Tenant tenantObj) {
        this.tenantObj = tenantObj;
    }
}