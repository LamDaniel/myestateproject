package myestatepackage;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * App class contains main method that displays the scenes and stage of the app
 * @author 1932789
 */
public class App extends Application {
    
    //Variables
    private static Scene scene;
    
    /**
     * start() initializes the scene and assigns it to the stage
     * @param stage
     * @throws IOException 
     */
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("main"), 1210, 603);
        stage.setTitle("MyEstate");
        stage.getIcons().add(new Image("images/logo.png"));
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * loadFXML() returns a Parent object of the FXML file
     * @param fxml
     * @return
     * @throws IOException 
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }
    
    /**
     * setRoot() takes Parent object based on String and sets it to scene
     * @param fxml
     * @throws IOException 
     */
    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }
    
    /**
     * setRoot() takes Parent object and sets it to scene
     * @param parentRoot
     * @throws IOException 
     */
    public static void setRoot(Parent parentRoot) throws IOException {
        scene.setRoot(parentRoot);
    } 
    
    /**
     * main() runs the app
     * @param args 
     */
    public static void main(String[] args) {
        launch();
    }

}