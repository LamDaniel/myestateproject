package myestatepackage;

/**
 * Lease class contains all the fields, getters and setters of a Lease Object
 * @author 1932789
 */
public class Lease {
    
    //Variables
    private int leaseId;
    private String startDate;
    private String endDate;
    
    //Constructor for Lease object
    public Lease(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public Lease(int leaseId, String startDate, String endDate) {
        this.leaseId = leaseId;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    //Getters
    public String getStartDate() {
        return startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public int getLeaseId() {
        return leaseId;
    }
    
    //toString() displays all fields of a Lease object
    @Override
    public String toString() {
        return "LEASE: " + this.getStartDate() + ", " + this.getEndDate(); 
    }
}
