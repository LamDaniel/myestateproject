package myestatepackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DBConnection initializes the connection to the MySQL database
 * @author Daniel Lam (1932789)
 */
public class DBConnection {
    
    //Variables
    private static final String username = "MyEstate";
    private static final String password = "Myestateadmin";
    private static final String url = "jdbc:mysql://127.0.0.1:3306/myestatedb";
    
    //getConnection() connects to the database
    public static Connection getConnection() {
        Connection dbConnection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConnection = DriverManager.getConnection(url, username, password);
        }
        catch (ClassNotFoundException e) {
            System.err.println("Driver Error Found.");
            System.out.println(e.getMessage());
        }
        catch (SQLException e) {
            System.err.println("SQL Connection Error Found.");
            e.printStackTrace();
        }
        catch (Exception e) {
            System.err.println("An Unknown Error has been Found.");
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }
}
