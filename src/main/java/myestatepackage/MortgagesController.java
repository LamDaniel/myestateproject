package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * MortgagesController is the controller that displays all mortgages and allows user to add, delete and modify Mortgages
 * @author Daniel Lam (1932789)
 */
public class MortgagesController extends MainController implements Initializable {
    
    //Variables
    @FXML private TableView<Mortgage> mortgagesTableView;
    @FXML private TableColumn<Mortgage, String> propertyNameCol;
    @FXML private TableColumn<Mortgage, String> paymentDateCol;
    @FXML private TableColumn<Mortgage, String> startDateCol;
    @FXML private TableColumn<Mortgage, String> endDateCol;
    @FXML private TableColumn<Mortgage, Double> downPaymentCol;
    ObservableList<Mortgage> mortgagesList = FXCollections.observableArrayList();
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            initializeColumns();
            loadMortgagesFromDB();
            mortgagesTableView.setItems(mortgagesList);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * loadTenantsFromDB() adds Mortgage objects from database into ObservableList
     * @throws SQLException 
     */
    private void loadMortgagesFromDB() throws SQLException {
        String propertiesStmt = "SELECT * FROM mortgage m INNER JOIN properties p ON p.mortgage_id = m.mortgage_id INNER JOIN bank_institution b ON b.bank_id = m.bank_id";
        PreparedStatement ps = conn.prepareStatement(propertiesStmt);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Bank bankObj = new Bank(rs.getInt("bank_id"), rs.getString("institution_name"), rs.getDouble("interest_rate"));
                    
            Mortgage mortgageObj = new Mortgage(rs.getInt("mortgage_id"), rs.getString("propertyName"), rs.getString("payment_date"), rs.getString("start_date"), rs.getString("end_date"), rs.getDouble("down_payment"), bankObj);
            System.out.println(mortgageObj.toString());
            mortgagesList.add(mortgageObj);
        }      
    }
    /**
     * initializeColumns() initializes columns of mortgagesTableView
     */
    private void initializeColumns() {
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<>("propertyName"));
        paymentDateCol.setCellValueFactory(new PropertyValueFactory<>("paymentDate"));
        startDateCol.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endDateCol.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        downPaymentCol.setCellValueFactory(new PropertyValueFactory<>("downPayment"));
    }
    
    /**
     * confirmRenew checks if a mortgage has been selected and changes view to viewMortgages
     * @throws IOException 
     */
    @FXML
    private void confirmRenew() throws IOException {
        Mortgage mortgageObj = mortgagesTableView.getSelectionModel().getSelectedItem();
        if (mortgageObj == null) {
            showErrorAlert("No Selected Tenant", "Please select a Mortgage first.", "");
        }
        else {
            FXMLLoader fxmlloader = new FXMLLoader(App.class.getResource("viewMortgages.fxml"));
            Parent parent = fxmlloader.load();
            
            ViewMortgagesController viewMortgagesController = (ViewMortgagesController) fxmlloader.getController();
            viewMortgagesController.setMortgage(mortgageObj);
            viewMortgagesController.displayFields(mortgageObj);
            App.setRoot(parent);
        }
    }
}

