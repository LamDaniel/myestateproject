package myestatepackage;

/**
 * Bank class contains all the fields, getters and setters of a Renovation Object
 * @author Daniel Lam (1932789)
 */
public class Bank {
    
    //Variables
    private int bankId;
    private String name;
    private double interest_rate;
   
    /**
     * Constructor for Bank object
     * @param bankId
     * @param name
     * @param interest_rate 
     */
    public Bank(int bankId, String name, double interest_rate) {
        this.bankId = bankId;
        this.name = name;
        this.interest_rate = interest_rate;
    }
    
    //Getters
    public String getName() {
        return name;
    }
    public double getInterestRate() {
        return this.interest_rate;
    }
    public int getBankId() {
        return bankId;
    }
    
    //toString() writes the fields of the Bank object as a string
    @Override
    public String toString() {
        return name + ": " + interest_rate + "%";
    }
    
    /**
     * equals() overrides the equals method to compare equality between two Bank Objects
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Bank) {
            Bank bankObj = (Bank) o;
            if (this.name.equals(bankObj.name) && this.interest_rate == bankObj.interest_rate) {
                return true;
            }
        }
        return false;
    }
}
