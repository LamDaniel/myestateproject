package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * ViewPropertiesController is the controller for modifying property
 * @author Daniel Lam (1932789)
 */
public class ViewPropertiesController extends PropertiesForm implements Initializable {

    //Variables
    private Property propertyObj;
    
    /**
     * setChoiceBoxTypes() initializes the choices for property types
     */
    @Override
    protected void setChoiceBoxTypes() {
        typeCB.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldType, String newType) {
                switch (newType) {
                    case "Condo":
                        condoFeesTf.setEditable(true);
                        condoFeesTf.setText(propertyObj.getCondoFees() + "");
                        nberOfUnitsTf.setEditable(false);
                        nberOfUnitsTf.setText(propertyObj.getNberOfUnits() + "");
                        break;
                    case "Plexes":
                        condoFeesTf.setEditable(false);
                        condoFeesTf.setText("0.0");
                        nberOfUnitsTf.setEditable(true);
                        nberOfUnitsTf.setText(propertyObj.getNberOfUnits() + "");
                        break;
                    case "House":
                        condoFeesTf.setEditable(false);
                        condoFeesTf.setText("0.0");
                        nberOfUnitsTf.setText("1");
                        nberOfUnitsTf.setEditable(false);
                        break;
                    default:
                        break;
                }
            }
        });
    }
    
    /**
     * modifyProperty() shows a confirm alert and if it gets accepted, modifies property
     * @throws SQLException
     * @throws IOException 
     */
    @FXML
    private void modifyProperty() throws SQLException, IOException {
        if (confirm("Confirm Modify", "Do you really want to modify this Property?")) {
            createTemplate();
            App.setRoot("properties");
        }
    }
    
    /**
     * create() calls methods to update property and insurance()
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        updateAll();
        System.out.println("Completed");
    }
    /**
     * updateAll() is helper method for updating property
     * @throws SQLException 
     */
    private void updateAll() throws SQLException {
        updateProperty();
        updateInsurance();   
    }
    /**
     * updateProperty() updates the Property in the Database
     * @throws SQLException 
     */
    private void updateProperty() throws SQLException {
        String updatePropertyStmt = "UPDATE Properties SET propertyName = ?, type = ?, address = ?, value = ?, nber_units = ?, rental_price = ?, property_tax_rate = ?, school_tax_rate = ?, condo_fees = ? WHERE property_id = ?";
        PreparedStatement ps = conn.prepareStatement(updatePropertyStmt);
        ps.setString(1, propertyNameTf.getText());
        ps.setString(2, typeCB.getValue());
        ps.setString(3, addressTf.getText());
        ps.setDouble(4, formatAmountOrRate(valueTf));
        ps.setInt(5, Integer.parseInt(nberOfUnitsTf.getText()));
        ps.setDouble(6, formatAmountOrRate(rental_priceTf));
        ps.setDouble(7, formatAmountOrRate(property_tax_rateTf));
        ps.setDouble(8, formatAmountOrRate(school_tax_rateTf));
        ps.setDouble(9, formatAmountOrRate(condoFeesTf));
        ps.setInt(10, propertyObj.getPropertyId());
        ps.execute();
        System.out.println("Updated Property.");
    }
    /**
     * updateInsurace() updates the Insurance in the Database
     * @throws SQLException 
     */
    private void updateInsurance() throws SQLException {
        String updateInsuranceStmt = "UPDATE Insurance SET name = ?, interest_rate = ? WHERE insurance_id = (SELECT insurance_id FROM Properties WHERE property_id = ?)";
        PreparedStatement ps = conn.prepareStatement(updateInsuranceStmt);
        ps.setString(1, insuranceNameTf.getText());
        ps.setDouble(2, formatAmountOrRate(interestRateTf));
        ps.setInt(3, propertyObj.getPropertyId());
        ps.execute();
        System.out.println("Updated Insurance.");
    }
    
    /**
     * setProperty() sets the propertyObj to this controller passed by the previous controller
     * @param propertyObj 
     */
    public void setProperty(Property propertyObj) {
        this.propertyObj = propertyObj;
    }
    
    /**
     * displayFields() shows all of the property's fields in the form
     * @param propertyName
     * @param type
     * @param address
     * @param value
     * @param rentalPrice
     * @param propertyTaxRate
     * @param schoolTaxRate
     * @param insuranceName
     * @param interestRate
     * @param condoFees
     * @param nberOfUnits 
     */
    public void displayFields(String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate, double condoFees, int nberOfUnits) {
        propertyNameTf.setText(propertyName);
        typeCB.setValue(type);
        addressTf.setText(address);
        valueTf.setText(value + "");
        rental_priceTf.setText(rentalPrice + "");
        property_tax_rateTf.setText(propertyTaxRate + "");
        school_tax_rateTf.setText(schoolTaxRate + "");
        insuranceNameTf.setText(insuranceName);
        interestRateTf.setText(interestRate + "");
        condoFeesTf.setText(condoFees + "");
        nberOfUnitsTf.setText(nberOfUnits + "");
    }
}