package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXML;

/**
 * AddRenovationsController shows form and methods to add renovations
 * @author 1932789
 */
public class AddRenovationsController extends RenovationForm {
    
    /**
     * addRenovation creates renovation
     */
    @FXML
    private void addRenovation() {
        createTemplate();
    }   
    
    /**
     * create() creates renovation object and inserts it into database
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        int contractorId = getContractorId();
        int propertyId = getPropertyId();
        
        String insertRenovationStmt = "INSERT INTO Renovations (renovation_id, contractor_id, property_id, cost, renovation_type, description, isFinished) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(insertRenovationStmt);
        ps.setString(1, null);
        ps.setInt(2, contractorId);
        ps.setInt(3, propertyId);
        ps.setDouble(4, formatAmountOrRate(getCostTf()));
        ps.setString(5, getTypeTf().getText());
        ps.setString(6, getDescriptionTa().getText());
        ps.setBoolean(7, translateToBoolean(getIsFinishedCB()));
        ps.execute();
    }
}
