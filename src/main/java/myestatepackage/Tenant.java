package myestatepackage;

/**
 * Tenant class contains all the fields, getters and setters of a Tenant Object
 * @author Daniel Lam (1932789)
 */
public class Tenant {
    
    //Variables
    private String fullName;
    private String propertyName;
    private String phoneNumber;
    private String emailAddress;
    private String rentStartDate;
    private String rentEndDate;
    private double yearlyIncome;
    private double incomeDebtRatio;
    private boolean hasPaidRent;
    private String paymentType;
    private int tenantId;
    private Lease tenantLease;
    
    //Constructor without tenantId
    public Tenant(String fullName, String propertyName, String phoneNumber, String emailAddress, String rentStartDate, String rentEndDate, double yearlyIncome, double incomeDebtRatio, boolean hasPaidRent, String paymentType, Lease tenantLease) {
        this.fullName = fullName;
        this.propertyName = propertyName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.rentStartDate = rentStartDate;
        this.rentEndDate = rentEndDate;
        this.yearlyIncome = yearlyIncome;
        this.incomeDebtRatio = incomeDebtRatio;
        this.hasPaidRent = hasPaidRent;
        this.paymentType = paymentType;
        this.tenantLease = tenantLease;
    }
    
    //Constructor with tenantId
    public Tenant(int tenantId, String fullName, String propertyName, String phoneNumber, String emailAddress, String rentStartDate, String rentEndDate, double yearlyIncome, double incomeDebtRatio, boolean hasPaidRent, String paymentType, Lease tenantLease) {
        this.tenantId = tenantId;
        this.fullName = fullName;
        this.propertyName = propertyName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.rentStartDate = rentStartDate;
        this.rentEndDate = rentEndDate;
        this.yearlyIncome = yearlyIncome;
        this.incomeDebtRatio = incomeDebtRatio;
        this.hasPaidRent = hasPaidRent;
        this.paymentType = paymentType;
        this.tenantLease = tenantLease;
    }
    
    //Getters
    public String getFullName() {
        return fullName;
    }
    public String getPropertyName() {
        return propertyName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public String getRentStartDate() {
        return rentStartDate;
    }
    public String getRentEndDate() {
        return rentEndDate;
    }
    public double getYearlyIncome() {
        return yearlyIncome;
    }
    public double getIncomeDebtRatio() {
        return incomeDebtRatio;
    }
    public boolean getHasPaidRent() {
        return hasPaidRent;
    }
    public String getPaymentType() {
        return paymentType;
    }
    public int getTenantId() {
        return tenantId;
    }
    public Lease getTenantLease() {
        return tenantLease;
    }
    
    //toString() returns a string with all the tenants fields when invoked
    @Override
    public String toString() {
        return "TENANT: " + this.getFullName() + ", " + this.getPropertyName() + ", " + this.getPhoneNumber() + ", " + this.getEmailAddress() + ", " + this.getRentStartDate() + ", " + this.getRentEndDate() + ", " + this.getYearlyIncome() + ", " + this.getIncomeDebtRatio() + ", " + this.getHasPaidRent() + ", " + this.getPaymentType() + ", " + this.getTenantLease() + ", ID: " + this.getTenantId();
    }
}
