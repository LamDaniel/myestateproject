package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * PropertiesController is the controller that displays all Properties and allows user to add, delete and modify Properties
 * @author Daniel Lam (1932789)
 */
public class PropertiesController extends MainController implements Initializable {
    
    //Variables
    @FXML private TableView<Property> propertiesTableView;
    @FXML private TableColumn<Property, String> propertyNameCol;
    @FXML private TableColumn<Property, String> typeCol;
    @FXML private TableColumn<Property, String> addressCol;
    @FXML private TableColumn<Property, Double> valueCol;
    @FXML private TableColumn<Property, Integer> nberUnitsCol;
    @FXML private TableColumn<Property, Double> rentalPriceCol;
    @FXML private TableColumn<Property, Double> propertyTaxRateCol;
    @FXML private TableColumn<Property, Double> schoolTaxRateCol;
    @FXML private TableColumn<Property, Double> condoFeesCol;
    ObservableList<Property> propertiesList = FXCollections.observableArrayList();
    
 
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            loadPropertiesFromDatabase();
            propertiesTableView.setItems(propertiesList);
            initializeColumns();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * initializeColumns() initialize columns of propertiesTableView
     */
    private void initializeColumns() {
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<>("propertyName"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        valueCol.setCellValueFactory(new PropertyValueFactory<>("value"));
        nberUnitsCol.setCellValueFactory(new PropertyValueFactory<>("nberOfUnits"));
        rentalPriceCol.setCellValueFactory(new PropertyValueFactory<>("rentalPrice"));
        propertyTaxRateCol.setCellValueFactory(new PropertyValueFactory<>("propertyTaxRate"));
        schoolTaxRateCol.setCellValueFactory(new PropertyValueFactory<>("schoolTaxRate"));
        condoFeesCol.setCellValueFactory(new PropertyValueFactory<>("condoFees"));
    }
    /**
     * loadTenantsFromDB() adds Properties objects from database into ObservableList
     * @throws SQLException 
     */
    private void loadPropertiesFromDatabase() throws SQLException {
        String propertiesStmt = "SELECT * FROM Properties p INNER JOIN Insurance i ON p.insurance_id = i.insurance_id;";
        PreparedStatement ps = conn.prepareStatement(propertiesStmt);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Property propertyObj = null;
            switch (rs.getString("type")) {
                case "Condo":
                    propertyObj = new Condo(rs.getInt("property_id"), rs.getString("propertyName"), rs.getString("type"), rs.getString("address"), rs.getDouble("value"), rs.getDouble("rental_price"), rs.getDouble("property_tax_rate"), rs.getDouble("school_tax_rate"), rs.getString("name"), rs.getDouble("interest_rate"), rs.getDouble("condo_fees"));
                    break;
                case "Plexes":
                    propertyObj = new Plex(rs.getInt("property_id"), rs.getString("propertyName"), rs.getString("type"), rs.getString("address"), rs.getDouble("value"), rs.getDouble("rental_price"), rs.getDouble("property_tax_rate"), rs.getDouble("school_tax_rate"), rs.getString("name"), rs.getDouble("interest_rate"), rs.getInt("nber_units"));
                    break;
                case "House":
                    propertyObj = new House(rs.getInt("property_id"), rs.getString("propertyName"), rs.getString("type"), rs.getString("address"), rs.getDouble("value"), rs.getDouble("rental_price"), rs.getDouble("property_tax_rate"), rs.getDouble("school_tax_rate"), rs.getString("name"), rs.getDouble("interest_rate"));
                    break;
                default:
                    break;
            }
            System.out.println(propertyObj.toString());
            propertiesList.add(propertyObj);
        }       
    }
    
    /**
     * switchToAddProperties changes view to addProperties
     * @throws IOException 
     */
    @FXML
    private void switchToAddProperties() throws IOException {
        App.setRoot("addProperties");
    }
    
    /**
     * switchToViewProperties checks if a Property has been selected and changes view to viewProperties
     * @throws IOException 
     */
    @FXML
    private void switchToViewProperties() throws IOException {
        Property propertyObj = propertiesTableView.getSelectionModel().getSelectedItem();
        if (checkSelectedProperty(propertyObj)) {
            FXMLLoader fxmlloader = new FXMLLoader(App.class.getResource("viewProperties.fxml"));
            Parent parent = fxmlloader.load();
            
            ViewPropertiesController viewPropertiesController = (ViewPropertiesController) fxmlloader.getController();
            viewPropertiesController.setProperty(propertyObj);
            viewPropertiesController.displayFields(propertyObj.getPropertyName(), propertyObj.getType(), propertyObj.getAddress(), propertyObj.getValue(), propertyObj.getRentalPrice(), propertyObj.getPropertyTaxRate(), propertyObj.getSchoolTaxRate(), propertyObj.getInsuranceName(), propertyObj.getInterestRate(), propertyObj.getCondoFees(), propertyObj.getNberOfUnits());
            App.setRoot(parent);
        }
    }
    
    /**
     * removeProperty() checks if a property has been chosen and deletes it
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    private void removeProperty() throws IOException, SQLException {
        Property propertyObj = propertiesTableView.getSelectionModel().getSelectedItem();
        System.out.println(propertyObj);
        if (checkSelectedProperty(propertyObj)) {
            if (confirm("Confirm Remove", "Do you really want to delete this Property?")) {
                deleteProperty(propertyObj);
                App.setRoot("properties");
            }
        }
    } 
    
    /**
     * checks if a PropertyObj has been selected in TableView
     * @param propertyObj
     * @return 
     */
    private boolean checkSelectedProperty(Property propertyObj) {
        try {
            if (propertyObj != null) {
                return true;
            }
            throw new NullPointerException("No Property Selected");
        }
        catch (NullPointerException e) {
            alert.getButtonTypes().removeAll();
            alert.setTitle("No Selected Property");
            alert.setHeaderText("Please select a Property first.");
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.showAndWait();
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * deleteProperty() deletes the chosen property from database
     * @param propertyObj
     * @throws SQLException 
     */
    private void deleteProperty(Property propertyObj) throws SQLException {
        conn.setAutoCommit(false);
        String deletePropertyStmt = "DELETE FROM Properties WHERE property_id = ?";
        String deleteMortgageStmt = "DELETE FROM Mortgage WHERE mortgage_id = (SELECT mortgage_id FROM Properties WHERE property_id = ?)";
        String deleteInsuranceStmt = "DELETE FROM Insurance WHERE insurance_id = (SELECT insurance_id FROM Properties WHERE property_id = ?)";
        ArrayList<String> statements = new ArrayList<>(Arrays.asList(deletePropertyStmt, deleteMortgageStmt, deleteInsuranceStmt));
        for (String statement : statements) {
            PreparedStatement ps = conn.prepareStatement(statement);
            ps.setInt(1, propertyObj.getPropertyId());
            ps.execute();
        }
        System.out.println("Property Deleted");
        conn.setAutoCommit(true);
    }
}
