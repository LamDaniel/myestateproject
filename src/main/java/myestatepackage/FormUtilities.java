package myestatepackage;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FormUtilities provides all the additional helper methods to all form classes
 * @author Daniel Lam (1932789)
 */
public class FormUtilities extends MainController {
    
    /**
     * formatAmountOrRate takes a TextField and cleans value to return a double
     * @param userInput
     * @return 
     */
    public double formatAmountOrRate(TextField userInput) {
        return Double.parseDouble(userInput.getText().replaceAll("[ ,$%]", ""));
    }
    
    /**
     * formatDate() takes a DatePicker, formats the value and returns it as a string
     * @param datePicker
     * @return String
     */
    public String formatDate(DatePicker datePicker) {
        return datePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
    
    /**
     * formatStringToDate() takes in a String and converts it into a LocalDate object
     * @param date
     * @return 
     */
    public LocalDate formatStringToDate(String date) {
        LocalDate formatter = LocalDate.parse(date);
        return formatter;
    }
    
    /**
     * translateBooleanToString() takes a boolean and translates it to Yes (true) or No (false)
     * @param hasPaidRent
     * @return String
     */
    public String translateBooleanToString(boolean hasPaidRent) {
        if (hasPaidRent) {
            return "Yes";
        }
        return "No";
    }
    
    /**
     * formatPhoneNumber() takes a phone number TextField, cleans the text and returns it as a String. If not, throws a SQLException
     * @param phoneNumberTf
     * @return String
     * @throws SQLException 
     */
    public String formatPhoneNumber(TextField phoneNumberTf) throws SQLException {
        String newPhoneNumber = phoneNumberTf.getText().replaceAll("[ ()-]", "");
        if (newPhoneNumber.length() == 10) {
            return newPhoneNumber;
        }
        throw new SQLException("Phone Number not valid");
    }
    
    /**
     * translateToBoolean() takes a Choice Box and if the value is equal to Yes then it returns true
     * @param hasPaidRentCB
     * @return boolean
     */
    public boolean translateToBoolean(ChoiceBox hasPaidRentCB) {
        return hasPaidRentCB.getValue().equals("Yes");
    }
}
