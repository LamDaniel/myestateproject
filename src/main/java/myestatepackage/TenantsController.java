package myestatepackage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * TenantsController is the controller that displays all Tenants and allows user to add, delete and modify Tenants
 * @author Daniel Lam (1932789)
 */
public class TenantsController extends MainController implements Initializable {
    
    //Variables
    @FXML private TableView<Tenant> tenantsTableView;
    @FXML private TableColumn<Tenant, String> fullNameCol;
    @FXML private TableColumn<Tenant, String> phoneNumberCol;
    @FXML private TableColumn<Tenant, String> emailAddressCol;
    @FXML private TableColumn<Tenant, String> rentStartDateCol;
    @FXML private TableColumn<Tenant, String> rentEndDateCol;
    @FXML private TableColumn<Tenant, Double> yearlyIncomeCol;
    @FXML private TableColumn<Tenant, Double> incomeDebtRatioCol;
    @FXML private TableColumn<Tenant, Boolean> hasPaidRentCol;
    @FXML private TableColumn<Tenant, String> paymentTypeCol;
    ObservableList<Tenant> tenantsList = FXCollections.observableArrayList();
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            loadTenantsFromDB();
            tenantsTableView.setItems(tenantsList);
            initializeColumns();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * loadTenantsFromDB() adds Tenant objects from database into ObservableList
     * @throws SQLException 
     */
    private void loadTenantsFromDB() throws SQLException {
        String loadTenantsStmt = "SELECT * FROM Tenants t INNER JOIN Lease l ON t.tenant_id = l.tenant_id INNER JOIN Properties p ON p.property_id = t.property_id";
        PreparedStatement ps = conn.prepareStatement(loadTenantsStmt);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Lease leaseObj = new Lease(rs.getString("start_date"), rs.getString("end_date"));
            Tenant tenantObj = new Tenant(rs.getInt("tenant_id"), rs.getString("fullname"), rs.getString("propertyName"), rs.getString("phone_number"), rs.getString("email"), rs.getString("rent_start_date"), rs.getString("rent_end_date"), rs.getDouble("yearly_income"), rs.getDouble("income_debt_ratio"), rs.getBoolean("hasPaidRent"), rs.getString("payment_type"), leaseObj);
            System.out.println(tenantObj);
            tenantsList.add(tenantObj);
        } 
    }
    /**
     * initializeColumns() initializes columns of tenantsTableView
     */
    private void initializeColumns() {
        fullNameCol.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        emailAddressCol.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
        rentStartDateCol.setCellValueFactory(new PropertyValueFactory<>("rentStartDate"));
        rentEndDateCol.setCellValueFactory(new PropertyValueFactory<>("rentEndDate"));
        yearlyIncomeCol.setCellValueFactory(new PropertyValueFactory<>("yearlyIncome"));
        incomeDebtRatioCol.setCellValueFactory(new PropertyValueFactory<>("incomeDebtRatio"));
        hasPaidRentCol.setCellValueFactory(new PropertyValueFactory<>("hasPaidRent"));
        paymentTypeCol.setCellValueFactory(new PropertyValueFactory<>("paymentType"));
    }
    
    /**
     * switchToAddTenants() changes view to addTenants
     * @throws IOException 
     */
    @FXML
    private void switchToAddTenants() throws IOException {
        App.setRoot("addTenants");
    }
    
    /**
     * switchToViewTenants() checks if a Tenant object has been selected and then changes view to viewTenants
     * @throws IOException 
     */
    @FXML
    private void switchToViewTenants() throws IOException {
        Tenant tenantObj = tenantsTableView.getSelectionModel().getSelectedItem();
        if (tenantObj == null) {
            showErrorAlert("No Selected Tenant", "Please select a Tenant first.", "");
        }
        else {
            FXMLLoader fxmlloader = new FXMLLoader(App.class.getResource("viewTenants.fxml"));
            Parent parent = fxmlloader.load();
            
            ViewTenantsController viewTenantsController = (ViewTenantsController) fxmlloader.getController();
            viewTenantsController.setTenant(tenantObj);
            viewTenantsController.displayFields(tenantObj.getFullName(), tenantObj.getPropertyName(), tenantObj.getEmailAddress(), tenantObj.getPhoneNumber(), tenantObj.getYearlyIncome(), tenantObj.getPaymentType(), tenantObj.getIncomeDebtRatio(), tenantObj.getRentStartDate(), tenantObj.getRentEndDate(), tenantObj.getTenantLease().getStartDate(), tenantObj.getTenantLease().getEndDate(), tenantObj.getHasPaidRent());
            App.setRoot(parent);
        }
    }
    
    /**
     * removeTenant() checks if a Tenant object has been selected and then removes selected Tenant
     * @throws SQLException
     * @throws IOException 
     */
    @FXML
    private void removeTenant() throws SQLException, IOException {
        Tenant tenantObj = tenantsTableView.getSelectionModel().getSelectedItem();
        if (tenantObj == null) {
            showErrorAlert("No Selected Tenant", "Please select a Tenant first.", "");
        }
        else {
            if (confirm("Confirm Remove", "Do you really want to delete this Tenant?")) {
                try {
                    removeAllFromDB(tenantObj);
                    switchToTenants();
                }
                catch (SQLException e) {
                    showErrorAlert("Database Error", "Error deleting from database.", "Please try again later.");
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * saveLease() saves a PDF file chosen by the user and saves inside files directory of the project
     * @throws IOException 
     */
    @FXML
    private void saveLease() throws IOException {
        try {
            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().add(new ExtensionFilter("PDF Files", "*.pdf"));
            List<File> listFiles = fc.showOpenMultipleDialog(null);
            for (File file : listFiles) {
                Files.copy(file.toPath(), new File("src/main/resources/files/" + file.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
            showInformationAlert("PDF have been saved", "PDF has successfully been saved!", "Please go into your project path and check /src/java/resources/files/ to find your lease.");
        }
        catch (Exception e) {
            showErrorAlert("Can't save PDF", "PDF could not be saved into your PC.", "Please check if the path /src/java/resources/files/ is available.");
            e.printStackTrace();
        }
    }
    /**
     * Helper method to remove in order
     * @param tenantObj
     * @throws SQLException 
     */
    private void removeAllFromDB(Tenant tenantObj) throws SQLException {
        conn.setAutoCommit(false);
        removeLeaseFromDB(tenantObj);
        removeTenantFromDB(tenantObj);
        conn.setAutoCommit(true);
    }
    /**
     * removeLeaseFromDB() remove lease from DB
     * @param tenantObj
     * @throws SQLException 
     */
    private void removeLeaseFromDB(Tenant tenantObj) throws SQLException {
        String deleteLeaseStmt = "DELETE FROM Lease WHERE tenant_id = ?";
        PreparedStatement ps = conn.prepareStatement(deleteLeaseStmt);
        ps.setInt(1, tenantObj.getTenantId());
        ps.execute();
    }
    /**
     * removeLeaseFromDB() remove Tenant from DB
     * @param tenantObj
     * @throws SQLException 
     */
    private void removeTenantFromDB(Tenant tenantObj) throws SQLException {
        String deleteTenantStmt = "DELETE FROM Tenants WHERE tenant_id = ?";
        PreparedStatement ps = conn.prepareStatement(deleteTenantStmt);
        ps.setInt(1, tenantObj.getTenantId());
        ps.execute();
    }
}
