package myestatepackage;

/**
 * Plex class is a sub-class that contains all the fields, getters and setters of a Property Object + nberOfUnits
 * @author 1932789
 */
public class Plex extends Property {
    
    //Variables
    private int nberOfUnits;   
    
    //Constrcutor without propertyId
    public Plex(String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate, int nberOfUnits) {
        super(propertyName, type, address, value, rentalPrice, propertyTaxRate, schoolTaxRate, insuranceName, interestRate);
        this.nberOfUnits = nberOfUnits;
    }   
    
    //Constructor with propertyId
    public Plex(int propertyId, String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate, int nberOfUnits) {
        super(propertyId, propertyName, type, address, value, rentalPrice, propertyTaxRate, schoolTaxRate, insuranceName, interestRate);
        this.nberOfUnits = nberOfUnits;
    } 
    
    //Getters
    @Override
    public int getNberOfUnits() {
        return nberOfUnits;
    }
    
    //toString() displays all the fields of a Plex object as a string
    @Override
    public String toString() {
        return "PLEX: " + this.getPropertyName() + ", " + this.getType() + ", " + this.getAddress() + ", " + this.getValue() + ", " + this.getRentalPrice() + ", " + this.getPropertyTaxRate() + ", " + this.getSchoolTaxRate() + ", " + this.getInsuranceName() + ", " + this.getInterestRate() + " - ID: " + this.getPropertyId();
    }
}
