package myestatepackage;

import java.io.IOException;
import java.sql.Connection;
import java.util.Optional;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 * MainController contains all the variables needed for all controllers, changing scenes methods and alerts
 * @author Daniel Lam (1932789)
 */
public class MainController implements Controller {
    
    //Variables
    protected Connection conn = DBConnection.getConnection();
    protected Alert alert = new Alert(AlertType.NONE);
    
    /**
     * switchToProperties() switches scenes to the properties view
     * @throws IOException 
     */
    @FXML
    @Override
    public void switchToProperties() throws IOException {
        App.setRoot("properties");
    }
    
    /**
     * switchToTenants() switches scenes to the tenants view
     * @throws IOException 
     */
    @FXML
    @Override
    public void switchToTenants() throws IOException {
        App.setRoot("tenants");
    }
    
    /**
     * switchToTenants() switches scenes to the tenants view
     * @throws IOException 
     */
    @FXML
    @Override
    public void switchToMortgages() throws IOException {
        App.setRoot("mortgages");
    }
    
    /**
     * switchToRenovations() switches scenes to the renovations view
     * @throws IOException 
     */
    @FXML
    @Override
    public void switchToRenovations() throws IOException {
        App.setRoot("renovations");
    }
    
    /**
     * switchToContractors() switches scenes to the contractors view
     * @throws IOException 
     */
    @FXML
    @Override
    public void switchToContractors() throws IOException {
        App.setRoot("contractors");
    }
    
    /**
     * switchToMain() switches scenes to the main view
     * @throws IOException 
     */
    @FXML
    @Override
    public void switchToMain() throws IOException {
        App.setRoot("main");
    }
    
    /**
     * confirm() displays the Confirmation Alert and returns true if user clicked on yes
     * @param titleText
     * @param headerText
     * @return 
     */
    protected boolean confirm(String titleText, String headerText) {
        ButtonType yes = new ButtonType("Yes");
        ButtonType no = new ButtonType("No");
        
        alert.setTitle(titleText);
        alert.setHeaderText(headerText);
        alert.setAlertType(Alert.AlertType.CONFIRMATION);
        alert.getButtonTypes().clear();
        alert.getButtonTypes().addAll(yes,no);
        
        Optional<ButtonType> option = alert.showAndWait();
        
        return option.get() == yes;
    }
    
    /**
     * showErrorAlert display an Error Alert with the title, header and content
     * @param titleText
     * @param headerText
     * @param contentText 
     */
    protected void showErrorAlert(String titleText, String headerText, String contentText) {
        alert.getButtonTypes().removeAll();
        alert.setTitle(titleText);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.setAlertType(Alert.AlertType.ERROR);
        alert.showAndWait();
    }
    
    protected void showInformationAlert(String titleText, String headerText, String contentText) {
        alert.getButtonTypes().removeAll();
        alert.setTitle(titleText);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.setAlertType(Alert.AlertType.INFORMATION);
        alert.showAndWait();
    }
}