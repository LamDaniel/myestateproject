package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * TenantsForm class is an abstract class that initializes all the variables and methods for any tenants form
 * @author Daniel Lam (1932789)
 */
public abstract class TenantsForm extends FormUtilities implements Initializable {
    
    //Variables
    @FXML protected TextField fullNameTf;
    @FXML protected TextField propertyNameTf;
    @FXML protected TextField emailTf;
    @FXML protected TextField phoneNumberTf;
    @FXML protected TextField yearlyIncomeTf;
    @FXML protected ChoiceBox paymentTypeCB;
    @FXML protected TextField incomeDebtRatioTf;
    @FXML protected DatePicker rentStartDatePicker;
    @FXML protected DatePicker rentEndDatePicker;
    @FXML protected DatePicker leaseStartDatePicker;
    @FXML protected DatePicker leaseEndDatePicker;
    @FXML protected ChoiceBox hasPaidRentCB;
    ObservableList<String> hasPaidRent = FXCollections.observableArrayList("Yes", "No");
    ObservableList<String> paymentType = FXCollections.observableArrayList("Cheque", "Debit", "Credit");
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadData();
    }
    
    /**
     * loadData() sets items of lists into choice boxes
     */
    private void loadData() {
         hasPaidRentCB.getItems().setAll(hasPaidRent);
         paymentTypeCB.getItems().setAll(paymentType);
    }
    
    /**
     * returnsToTenants() sends user back to tenants.fxml
     * @throws IOException 
     */
    @FXML
    private void returnToTenants() throws IOException {
        App.setRoot("tenants");
    }
    
    /**
     * createTemplate() calls create() and catches any errors
     */
    protected void createTemplate() {
        try {
            conn.setAutoCommit(false);
            create();
            conn.setAutoCommit(true);
            App.setRoot("tenants");
        }
        catch (NullPointerException|NumberFormatException e) {
            showErrorAlert("Can't be created", "Tenant can't be created.", "Please recheck for any missing cases and incorrect formats.");
            e.printStackTrace();
        }
        catch (SQLIntegrityConstraintViolationException e){
            showErrorAlert("Invalid Property Name", "Property name couldn't be found.", "Please retype the correct Property Name (including capital letters)");
            e.printStackTrace();
        }
        catch (SQLException e) {
            showErrorAlert("Database Error", "Tenant could not be inserted into the Database", "Please recheck for any missing cases and incorrect formats.)");
            e.printStackTrace();
        }
        catch (Exception e) {
            showErrorAlert("Unknown Error", "An Unknown Error Occured.", "Please try again.");
            e.printStackTrace();
        }
    }
    
    /**
     * create() is abstract method that runs the methods to create property
     * @throws SQLException
     * @throws IOException 
     */
    protected abstract void create() throws SQLException, IOException;
    
    //Getters
    public TextField getFullNameTf() {
        return fullNameTf;
    }
    public TextField getPropertyNameTf() {
        return propertyNameTf;
    }
    public TextField getEmailTf() {
        return emailTf;
    }
    public TextField getPhoneNumberTf() {
        return phoneNumberTf;
    }
    public TextField getYearlyIncomeTf() {
        return yearlyIncomeTf;
    }
    public ChoiceBox getPaymentTypeCB() {
        return paymentTypeCB;
    }
    public TextField getIncomeDebtRatioTf() {
        return incomeDebtRatioTf;
    }
    public DatePicker getRentStartDatePicker() {
        return rentStartDatePicker;
    }
    public DatePicker getRentEndDatePicker() {
        return rentEndDatePicker;
    }
    public DatePicker getLeaseStartDatePicker() {
        return leaseStartDatePicker;
    }
    public DatePicker getLeaseEndDatePicker() {
        return leaseEndDatePicker;
    }
    public ChoiceBox getHasPaidRentCB() {
        return hasPaidRentCB;
    }
}
