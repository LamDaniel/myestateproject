package myestatepackage;

/**
 * Mortgage class contains all the fields, getters and setters of a Mortgage Object
 * @author 1932789
 */
public class Mortgage {
    
    //Variables
    private int mortgageId;
    private String propertyName;
    private String paymentDate;
    private String startDate;
    private String endDate;
    private double downPayment;
    private Bank bankObj;
    
    //Constructor with mortgageId
    public Mortgage(int mortgageId, String propertyName, String paymentDate, String startDate, String endDate, double downPayment, Bank bankObj) {
        this.mortgageId = mortgageId;
        this.propertyName = propertyName;
        this.paymentDate = paymentDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.downPayment = downPayment;
        this.bankObj = bankObj;
    }
    
    //Constructor without mortgageId
    public Mortgage(String propertyName, String paymentDate, String startDate, String endDate, double downPayment) {
        this.propertyName = propertyName;
        this.paymentDate = paymentDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.downPayment = downPayment;
    }
    
    //Getters
    public int getMortgageId() {
        return mortgageId;
    }
    public String getPropertyName() {
        return propertyName;
    }
    public String getPaymentDate() {
        return paymentDate;
    }
    public String getStartDate() {
        return startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public double getDownPayment() {
        return downPayment;
    }
    public Bank getBank() {
        return bankObj;
    }
    
    //toString() displays the fields of a Mortgage Object
    @Override
    public String toString() {
        return "Mortgage: " + propertyName + ", " + paymentDate + ", " + startDate + ", " + endDate + ", " + downPayment + ", " + bankObj + ", ID: " + mortgageId;
    }
}
