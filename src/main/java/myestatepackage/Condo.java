package myestatepackage;

/**
 * Condo class is a sub-class that contains all the fields, getters and setters of a Property Object + condoFees
 * @author 1932789
 */
public class Condo extends Property {
    
    //Variables
    private double condoFees;  
    
    //Constructor with propertyId
    public Condo(int propertyId, String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate, double condoFees) {
        super(propertyId, propertyName, type, address, value, rentalPrice, propertyTaxRate, schoolTaxRate, insuranceName, interestRate);
        this.condoFees = condoFees;
    }  
    
    //Constructor without propertyId
    public Condo(String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate, double condoFees) {
        super(propertyName, type, address, value, rentalPrice, propertyTaxRate, schoolTaxRate, insuranceName, interestRate);
        this.condoFees = condoFees;
    }   
    
    //Getters
    @Override
    public double getCondoFees() {
        return condoFees;
    }
    
    //toString() dispalys all the fields of a Condo Object
    @Override
    public String toString() {
        return "CONDO: " + this.getPropertyName() + ", " + this.getType() + ", " + this.getAddress() + ", " + this.getValue() + ", " + this.getRentalPrice() + ", " + this.getPropertyTaxRate() + ", " + this.getSchoolTaxRate() + ", " + this.getInsuranceName() + ", " + this.getInterestRate() + ", " + condoFees + " - ID: " + this.getPropertyId();
    }
}
