package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

/**
 * PropertiesForm contains all the variables and methods for any form with properties
 * @author Daniel Lam (1932789)
 */
public abstract class PropertiesForm extends FormUtilities implements Initializable {
    
    //Variables
    @FXML protected TextField propertyNameTf;
    @FXML protected TextField addressTf;
    @FXML protected TextField valueTf;
    @FXML protected ChoiceBox<String> typeCB;
    @FXML protected TextField rental_priceTf;
    @FXML protected TextField property_tax_rateTf;
    @FXML protected TextField school_tax_rateTf;
    @FXML protected TextField insuranceNameTf;
    @FXML protected TextField interestRateTf;
    @FXML protected TextField condoFeesTf;
    @FXML protected TextField nberOfUnitsTf;
    ObservableList<String> typeList = FXCollections.observableArrayList("House", "Condo", "Plexes");
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadData();
        setChoiceBoxTypes();
    }
    /**
     * loadData() displays the data of lists into choice boxes
     */
    private void loadData() {
        typeCB.getItems().setAll(typeList);    
    }
    /**
     * setChoiceBoxTypes() checks if there's a change on the choice box and edits the text fields accordingly
     */
    protected void setChoiceBoxTypes() {
        typeCB.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldType, String newType) {
                System.out.println(newType);
                switch (newType) {
                    case "Condo":
                        condoFeesTf.setEditable(true);
                        condoFeesTf.setText("");
                        nberOfUnitsTf.setEditable(false);
                        nberOfUnitsTf.setText("1");
                        break;
                    case "Plexes":
                        condoFeesTf.setEditable(false);
                        condoFeesTf.setText("0.00");
                        nberOfUnitsTf.setEditable(true);
                        nberOfUnitsTf.setText("");
                        break;
                    case "House":
                        condoFeesTf.setEditable(false);
                        condoFeesTf.setText("0.00");
                        nberOfUnitsTf.setText("1");
                        nberOfUnitsTf.setEditable(false);
                        break;
                    default:
                        break;
                }
            }
        });
    }
    
    /**
     * createTemplate() calls create() and catches any errors
     */
    protected void createTemplate() {
        try {
            conn.setAutoCommit(false);
            create();
            conn.setAutoCommit(true);
        }
        catch (NullPointerException|NumberFormatException e) {
            showErrorAlert("Can't be created", "Property can't be created or modified.", "Please recheck for any missing cases and incorrect formats.");
            e.printStackTrace();
        }
        catch (SQLIntegrityConstraintViolationException e) {
            showErrorAlert("Existing Property", "Property already exists in the database.", "Please name your property differently.");
            e.printStackTrace();
        }
        catch (SQLException e) {
            showErrorAlert("Database error", "Error while inserting into database.", "Please recheck for any incorrect formats and try again.");
            e.printStackTrace();
        }
        catch (Exception e) {
            showErrorAlert("Unknown Error", "An Unknown Error Occured.", "Please try again.");
            e.printStackTrace();
        }
    }
    
    /**
     * create() is abstract method that runs the methods to create property
     * @throws SQLException
     * @throws IOException 
     */
    protected abstract void create() throws SQLException, IOException;
    
    /**
     * returnToProperties returns the user to the properties page
     * @throws IOException 
     */
    @FXML
    protected void returnToProperties() throws IOException {
        App.setRoot("properties");
    }
    
    //Getters
    public TextField getPropertyNameTf() {
        return propertyNameTf;
    }
    public TextField getAddressTf() {
        return addressTf;
    }
    public TextField getValueTf() {
        return valueTf;
    }
    public ChoiceBox getTypeCB() {
        return typeCB;
    }
    public TextField getRentalPriceTf() {
        return rental_priceTf;
    }
    public TextField getPropertyTaxRateTf() {
        return property_tax_rateTf;
    }
    public TextField getSchoolTaxRateTf() {
        return school_tax_rateTf;
    }
    public TextField getInsuranceNameTf() {
        return insuranceNameTf;
    }
    public TextField getInterestRateTf() {
        return interestRateTf;
    }
    public TextField getCondoFeesTf() {
        return condoFeesTf;
    }
    public TextField getNberOfUnitsTf() {
        return nberOfUnitsTf;
    }
}
