package myestatepackage;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ContractorsController extends MainController implements Initializable {
    
    //Variables
    @FXML private TableView<Contractor> contractorsTableView;
    @FXML private TableColumn<Contractor, String> contractorNameCol;
    @FXML private TableColumn<Contractor, String> professionCol;
    @FXML private TableColumn<Contractor, String> companyNameCol;
    @FXML private TableColumn<Contractor, Double> phoneNumberCol;
    @FXML private TableColumn<Contractor, Integer> emailAddressCol;
    ObservableList<Contractor> contractorsList = FXCollections.observableArrayList();
    
    /**
     * initialize() runs the methods when controller gets created
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            loadContractorsFromDB();
            contractorsTableView.setItems(contractorsList);
            initializeColumns();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * loadTenantsFromDB() adds Contractors objects from database into ObservableList
     * @throws SQLException 
     */
    private void loadContractorsFromDB() throws SQLException {
        String loadContractorsStmt = "SELECT * FROM Contractors";
        PreparedStatement ps = conn.prepareStatement(loadContractorsStmt);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Contractor contractorObj = new Contractor(rs.getInt("contractor_id"), rs.getString("name"), rs.getString("company"), rs.getString("phone_number"), rs.getString("email"), rs.getString("profession"));
            contractorsList.add(contractorObj);
        }
    }
    private void initializeColumns() {
        contractorNameCol.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        professionCol.setCellValueFactory(new PropertyValueFactory<>("profession"));
        companyNameCol.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        emailAddressCol.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
    }
    @FXML
    private void switchToAddContractors() throws IOException {
        App.setRoot("addContractors");
    }
    
    @FXML
    private void switchToViewContractors() throws IOException {
        Contractor contractorObj = contractorsTableView.getSelectionModel().getSelectedItem();
        if (contractorObj == null) {
            showErrorAlert("No Selected Contractor", "Please select a Contractor first.", "");
        }
        else {
            FXMLLoader fxmlloader = new FXMLLoader(App.class.getResource("viewContractors.fxml"));
            Parent parent = fxmlloader.load();
            
            ViewContractorsController viewContractorsController = (ViewContractorsController) fxmlloader.getController();
            viewContractorsController.setContractor(contractorObj);
            viewContractorsController.displayFields(contractorObj);
            App.setRoot(parent);
        }
    }
    
    
    @FXML
    private void confirmRemove() throws IOException {
        Contractor contractorObj = contractorsTableView.getSelectionModel().getSelectedItem();
        if (contractorObj == null) {
            showErrorAlert("No Selected Contractor", "Please select a Contractor first.", "");
        }
        else {
            try {
                if (confirm("Delete Contractor", "Are you sure you want to delete this contractor?")) {
                    removeContractorFromDB(contractorObj);
                    App.setRoot("contractors");
                }
            }
            catch (SQLException e) {
                showErrorAlert("Database Error", "Error deleting from database.", "Please try again later.");
                e.printStackTrace();
            }
        }
    }
    private void removeContractorFromDB(Contractor contractorObj) throws SQLException {
        conn.setAutoCommit(false);
        String removeContractorFromDB = "DELETE FROM Contractors WHERE contractor_id = ?";
        PreparedStatement ps = conn.prepareStatement(removeContractorFromDB);
        ps.setInt(1, contractorObj.getContractorId());
        ps.execute();
        conn.setAutoCommit(true);
    }
}
