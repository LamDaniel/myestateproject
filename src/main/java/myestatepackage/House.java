/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myestatepackage;

/**
 * House class is a sub-class that contains all the fields, getters and setters of a Property Object
 * @author 1932789
 */
public class House extends Property {
    
    //Constructor with propertyId
    public House(int propertyId, String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate) {
        super(propertyId, propertyName, type, address, value, rentalPrice, propertyTaxRate, schoolTaxRate, insuranceName, interestRate);
    }
    
    //Constructor without propertyId
    public House(String propertyName, String type, String address, double value, double rentalPrice, double propertyTaxRate, double schoolTaxRate, String insuranceName, double interestRate) {
        super(propertyName, type, address, value, rentalPrice, propertyTaxRate, schoolTaxRate, insuranceName, interestRate);
    }  
    
    //toString() displays all the fields of a House object
    @Override
    public String toString() {
        return "HOUSE: " + this.getPropertyName() + ", " + this.getType() + ", " + this.getAddress() + ", " + this.getValue() + ", " + this.getRentalPrice() + ", " + this.getPropertyTaxRate() + ", " + this.getSchoolTaxRate() + ", " + this.getInsuranceName() + ", " + this.getInterestRate() + " - ID: " + this.getPropertyId();
    }
}
