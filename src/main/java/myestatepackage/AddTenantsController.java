package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * AddTenantsController shows form and methods to add tenants
 * @author 1932789
 */
public class AddTenantsController extends TenantsForm implements Initializable {
    
    //Variables
    private Tenant tenantObj;
    
    /**
     * addTenant() creates tenant
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    private void addTenant() throws IOException, SQLException {
        createTemplate();
    }
    
    /**
     * create() creates lease and tenant objects
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        Lease leaseObj = new Lease(formatDate(leaseStartDatePicker), formatDate(leaseEndDatePicker));
        System.out.println(leaseObj);
        Tenant newTenantObj = new Tenant(fullNameTf.getText(), propertyNameTf.getText(), formatPhoneNumber(phoneNumberTf), emailTf.getText(), formatDate(rentStartDatePicker), formatDate(rentEndDatePicker), formatAmountOrRate(yearlyIncomeTf), formatAmountOrRate(incomeDebtRatioTf), translateToBoolean(hasPaidRentCB), paymentTypeCB.getValue().toString(), leaseObj); 
        System.out.println(newTenantObj);
        this.tenantObj = newTenantObj;
        
        insertIntoDB();
    }
    /**
     * insertIntoDB() is a helper method that inserts lease and tenant into database
     * @throws SQLException 
     */
    private void insertIntoDB() throws SQLException {
        insertTenantIntoDB();
        insertLeaseIntoDB();
    }
    
    /**
     * insertTenantIntoDB inserts tenant into database
     * @throws SQLException 
     */
    private void insertTenantIntoDB() throws SQLException {
        int propertyId = getPropertyId();
        
        String tenantStmt = "INSERT INTO Tenants (tenant_id, property_id, fullname, phone_number, email, rent_start_date, rent_end_date, yearly_income, income_debt_ratio, hasPaidRent, payment_type) \n"
                            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        System.out.println(tenantStmt);
        PreparedStatement ps = conn.prepareStatement(tenantStmt);
        ps.setString(1, null);
        ps.setInt(2, propertyId);
        ps.setString(3, tenantObj.getFullName());
        ps.setString(4, tenantObj.getPhoneNumber());
        ps.setString(5, tenantObj.getEmailAddress());
        ps.setString(6, tenantObj.getRentStartDate());
        ps.setString(7, tenantObj.getRentEndDate());
        ps.setDouble(8, tenantObj.getYearlyIncome());
        ps.setDouble(9, tenantObj.getIncomeDebtRatio());
        ps.setBoolean(10, tenantObj.getHasPaidRent());
        ps.setString(11, tenantObj.getPaymentType());
        ps.execute();

    }
    /**
     * getPropertyId() returns propertyId of appropriate property
     * @return
     * @throws SQLException 
     */
    private int getPropertyId() throws SQLException {
        int propertyId = 0;
        String propertyIdStmt = "SELECT property_id FROM Properties WHERE propertyName = ?";
        PreparedStatement ps = conn.prepareStatement(propertyIdStmt);
        ps.setString(1, tenantObj.getPropertyName());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            propertyId = rs.getInt("property_id");
        }
        return propertyId;
    }
    /**
     * insertLeaseIntoDB() inserts lease into database
     * @throws SQLException 
     */
    private void insertLeaseIntoDB() throws SQLException {
        int tenantId = getTenantId();
        
        String leaseStmt = "INSERT INTO Lease (lease_id, tenant_id, start_date, end_date) VALUES (?, ?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(leaseStmt);
        ps.setString(1, null);
        ps.setInt(2, tenantId);
        ps.setString(3, tenantObj.getTenantLease().getStartDate());
        ps.setString(4, tenantObj.getTenantLease().getEndDate());
        ps.execute();
    }
    /**
     * getTenantId() retrieves id of last inserted object which is tenant
     * @return
     * @throws SQLException 
     */
    private int getTenantId() throws SQLException {
        int tenantId = 0;
        String tenantIdStmt = "SELECT last_insert_id()";
        PreparedStatement ps = conn.prepareStatement(tenantIdStmt);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            tenantId = rs.getInt("last_insert_id()");
        }
        return tenantId;
    }
}