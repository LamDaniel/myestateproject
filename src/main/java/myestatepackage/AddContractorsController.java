package myestatepackage;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXML;

/**
 * AddContractorsController shows form and methods to add contractors 
 * @author 1932789
 */
public class AddContractorsController extends ContractorForm {
    
    /**
     * addContractor() adds contractor
     * @throws IOException 
     */
    @FXML
    private void addContractor() throws IOException {
        createTemplate();
    }
    
    /**
     * create() creates contractor object
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    protected void create() throws SQLException, IOException {
        Contractor contractorObj = new Contractor(getContractorNameTf().getText(), getCompanyNameTf().getText(), formatPhoneNumber(getPhoneNumberTf()), getEmailAddressTf().getText(), getProfessionTf().getText());
        
        insertContractorIntoDB(contractorObj);
    }
    
    /**
     * insertContractorIntoDB() inserts contractor into database
     * @param contractorObj
     * @throws SQLException 
     */
    private void insertContractorIntoDB(Contractor contractorObj) throws SQLException {
        String insertContractorStmt = "INSERT INTO Contractors (contractor_id, name, company, phone_number, email, profession) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(insertContractorStmt);
        ps.setString(1, null);
        ps.setString(2, contractorObj.getFullName());
        ps.setString(3, contractorObj.getCompanyName());
        ps.setString(4, contractorObj.getPhoneNumber());
        ps.setString(5, contractorObj.getEmailAddress());
        ps.setString(6, contractorObj.getProfession());
        ps.execute();
    }
}