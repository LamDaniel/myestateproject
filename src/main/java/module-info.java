module myestatepackage {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens myestatepackage to javafx.fxml, java.sql;
    exports myestatepackage;
}
