CREATE DATABASE IF NOT EXISTS MyEstateDB;
USE MyEstateDB;

DROP TABLE IF EXISTS tenants;
DROP TABLE IF EXISTS renovations;
DROP TABLE IF EXISTS properties;
DROP TABLE IF EXISTS mortgage;
DROP TABLE IF EXISTS bank_institution;
DROP TABLE IF EXISTS contractors;
DROP TABLE IF EXISTS insurance;
DROP TABLE IF EXISTS lease;

CREATE TABLE Insurance (
	insurance_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    interest_rate DECIMAL(5,2) UNSIGNED CHECK (interest_rate <= 100),
    PRIMARY KEY (`insurance_id`)
);

CREATE TABLE Bank_Institution (
	bank_id INT NOT NULL AUTO_INCREMENT,
    institution_name VARCHAR(128) NOT NULL,
    interest_rate DECIMAL(5,2) UNSIGNED CHECK (interest_rate <= 100) NOT NULL,
    PRIMARY KEY (`bank_id`)
);

CREATE TABLE Contractors (
	contractor_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    company VARCHAR(128),
    phone_number CHAR(14),
    email VARCHAR(128),
    profession VARCHAR(128),
    PRIMARY KEY (`contractor_id`)
); 

CREATE TABLE Lease (
	lease_id INT NOT NULL AUTO_INCREMENT,
    tenant_id INT NOT NULL,
    start_date DATE,
    end_date DATE,
    PRIMARY KEY (`lease_id`)
);

CREATE TABLE Mortgage (
	mortgage_id INT NOT NULL AUTO_INCREMENT,
    bank_id INT NOT NULL,
    payment_date DATE,
    start_date DATE,
    end_date DATE,
    down_payment DECIMAL(10,2) UNSIGNED,
    PRIMARY KEY (`mortgage_id`),
    FOREIGN KEY (`bank_id`) REFERENCES Bank_Institution(`bank_id`) ON DELETE CASCADE
);

CREATE TABLE Properties (
	property_id INT NOT NULL AUTO_INCREMENT,
    insurance_id INT NOT NULL,
    mortgage_id INT NOT NULL,
    propertyName varchar(128) UNIQUE,
    type VARCHAR(128),
    address VARCHAR(128),
    value DECIMAL(10,2) UNSIGNED,
    nber_units INT UNSIGNED,
    rental_price DECIMAL(10,2) UNSIGNED,
    property_tax_rate DECIMAL(5,2) UNSIGNED CHECK (property_tax_rate <= 100),
    school_tax_rate DECIMAL(5,2) UNSIGNED CHECK (school_tax_rate <= 100),
	condo_fees DECIMAL(10,2) UNSIGNED,
    PRIMARY KEY (`property_id`),
    FOREIGN KEY (`insurance_id`) REFERENCES Insurance(`insurance_id`) ON DELETE CASCADE,
    FOREIGN KEY (`mortgage_id`) REFERENCES Mortgage(`mortgage_id`) ON DELETE CASCADE
);

CREATE TABLE Renovations (
	renovation_id INT NOT NULL AUTO_INCREMENT,
    contractor_id INT NOT NULL,
    property_id INT NOT NULL,
    cost DECIMAL(10,2) UNSIGNED,
    renovation_type VARCHAR(128),
    description VARCHAR(128),
    isFinished BOOLEAN NOT NULL,
    PRIMARY KEY (`renovation_id`),
    FOREIGN KEY (`contractor_id`) REFERENCES Contractors(`contractor_id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES Properties(`property_id`) ON DELETE CASCADE
);

CREATE TABLE Tenants (
	tenant_id INT NOT NULL AUTO_INCREMENT,
    property_id INT NOT NULL,
    fullname VARCHAR(128) NOT NULL,
    phone_number CHAR(14),
    email VARCHAR(128),
    rent_start_date DATE,
    rent_end_date DATE,
    yearly_income DECIMAL(10,2) UNSIGNED,
    income_debt_ratio DECIMAL(5,2) UNSIGNED CHECK (income_debt_ratio <= 100),
    hasPaidRent BOOLEAN NOT NULL,
    payment_type VARCHAR(128) NOT NULL,
    PRIMARY KEY (`tenant_id`),
    FOREIGN KEY (`property_id`) REFERENCES Properties(`property_id`) ON DELETE CASCADE
);

ALTER TABLE Mortgage auto_increment = 1;
ALTER TABLE insurance auto_increment = 1;
ALTER TABLE properties auto_increment = 1;
ALTER TABLE tenants auto_increment = 1;
ALTER TABLE lease auto_increment = 1;
ALTER TABLE renovations auto_increment = 1;
ALTER TABLE contractors auto_increment = 1;

DROP PROCEDURE IF EXISTS insertBanks;
DELIMITER //
CREATE PROCEDURE insertBanks()
BEGIN
	DECLARE count INT DEFAULT 0;
    SELECT COUNT(*) INTO count FROM bank_institution;
    IF count = 0 THEN
		INSERT INTO bank_institution VALUES (null, "Bank of Montreal", 1.00);
		INSERT INTO bank_institution VALUES (null, "Royal Bank of Canada", 1.25);
		INSERT INTO bank_institution VALUES (null, "TD Canada Trust", 2.00);
		INSERT INTO bank_institution VALUES (null, "Scotiabank", 0.75);
		INSERT INTO bank_institution VALUES (null, "Canadian Imperial Bank of Commerce", 1.50);
    END IF;
END //
DELIMITER ;
CALL insertBanks();

SHOW processlist;