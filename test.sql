USE myestatedb;
SELECT * FROM mortgage;
SELECT * FROM properties;
SELECT * FROM insurance;
SELECT * FROM bank_institution;
SELECT * FROM mortgage m
INNER JOIN properties p ON p.mortgage_id = m.mortgage_id;
SELECT * FROM properties;
SELECT last_insert_id();
SELECT * FROM Tenants;
SELECT * FROM Lease;
SELECT * FROM renovations;
SELECT l.lease_id FROM Lease l WHERE tenant_id = 2;
SELECT lease_id FROM lease WHERE tenant_id = 2;
SELECT * FROM bank_institution b INNER JOIN mortgage m ON m.bank_id = b.bank_id WHERE m.mortgage_id = 1;
SELECT * FROM mortgage m INNER JOIN properties p ON p.mortgage_id = m.mortgage_id INNER JOIN bank_institution b ON b.bank_id = m.bank_id;
SELECT * FROM contractors;

INSERT INTO bank_institution VALUES (null, "Bank of Montreal", 1.00);
INSERT INTO bank_institution VALUES (null, "Royal Bank of Canada", 1.25);
INSERT INTO bank_institution VALUES (null, "TD Canada Trust", 2.00);
INSERT INTO bank_institution VALUES (null, "Scotiabank", 0.75);
INSERT INTO bank_institution VALUES (null, "Canadian Imperial Bank of Commerce", 1.50);
-- INSERT INTO mortgage VALUES (null, 2, "2021-04-24", "2021-04-24", "2021-05-24", 100.50);
INSERT INTO properties (property_id, insurance_id, mortgage_id, propertyName, type, address, value, nber_units, rental_price, property_tax_rate, school_tax_rate, condo_fees) 
VALUES (null, ?, ?, ?, ?, ?, ?, IFNULL(?, DEFAULT(`1`)), ?, ?, ?, IFNULL(?, DEFAULT(`0`)));

DELETE FROM properties;
DELETE FROM Mortgage;
DELETE FROM insurance;

ALTER TABLE Mortgage auto_increment = 1;
ALTER TABLE insurance auto_increment = 1;
ALTER TABLE properties auto_increment = 1;
ALTER TABLE renovations RENAME COLUMN value TO cost;

SELECT contractor_id FROM Contractors WHERE name = "testContractor2";
SELECT * FROM Renovations r INNER JOIN Properties p ON p.property_id = r.property_id INNER JOIN Contractors c ON c.contractor_id = r.contractor_id;
SELECT * FROM Tenants t INNER JOIN Lease l ON t.tenant_id = l.tenant_id INNER JOIN properties p ON p.property_id = t.property_id; 
SELECT * FROM Properties p INNER JOIN Insurance i ON p.insurance_id = i.insurance_id;
SELECT mortgage_id FROM properties WHERE property_id = 1;
SELECT property_id FROM properties;
SELECT insurance_id FROM properties WHERE property_id = 1; 
DELETE FROM Insurance WHERE insurance_id = 7;

DELETE p FROM Properties p INNER JOIN Mortgage m ON p.mortgage_id = m.mortgage_id INNER JOIN Insurance i ON i.insurance_id = p.insurance_id INNER JOIN Tenants t ON t.property_id = p.property_id INNER JOIN Lease l ON l.tenant_id = t.tenant_id WHERE p.property_id = 14;